package LoadTester2;

import LoadTester2.actions.CreateGroup;

public class Group {
    public long groupId;
    public String name;
    public String description;
    public boolean isPrivate;
    public long adminId;

    public Group () {

    }

    public Group(CreateGroup.CreateGroupResponse response) {
        this.groupId = response.id;
        this.name = response.name;
        this.description = response.description;
        this.isPrivate = response.isPrivate;
        this.adminId = response.adminId;
    }
}
