package LoadTester2;


import LoadTester.Utils;
import LoadTester2.actions.CreateGroup;

import java.util.*;

public class User {
    public String username = "";
    public String password;
    public String email;
    public long userId;

    public int cityId;
    public int ageId;
    public int genderId;
    public ArrayList<Long> music;
    public ArrayList<Long> atmosphere;
    public int spendingLimit;
    public boolean notifyOfGroupInvites = true;
    public String profilePictureUrl = null;

    public Map<Long, Group> userGroups = new HashMap<Long, Group>();

    private static Random rand = new Random();
    private static String alphabet = "abcdefghijklmnopqrstuvwxyz0123456789";

    public static User generateRandom() {
        User user = new User();
        for (int i = 0; i < 10; i++) {
            user.username += alphabet.charAt(rand.nextInt(35));
        }
        user.email = UUID.randomUUID().toString().substring(15);
        user.password = "password";

        user.cityId = 0;
        user.genderId = rand.nextInt(1);
        user.ageId = rand.nextInt(4);
        user.music = Utils.getRandomMusicArrayList();
        user.atmosphere = Utils.getRandomAtmosphereArrayList();
        user.spendingLimit = rand.nextInt(3);
        return user;
    }

    public void addGroup(CreateGroup.CreateGroupResponse response) {
        userGroups.put(response.id, new Group(response));
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", userId=" + userId +
                ", cityId=" + cityId +
                ", ageId=" + ageId +
                ", genderId=" + genderId +
                ", music=" + music +
                ", atmosphere=" + atmosphere +
                ", spendingLimit=" + spendingLimit +
                ", notifyOfGroupInvites=" + notifyOfGroupInvites +
                ", profilePictureUrl='" + profilePictureUrl + '\'' +
                ", userGroups=" + userGroups +
                '}';
    }
}
