package LoadTester2.Request.params;


public class SearchVenueInGroup {
    public Long groupId;
    public String search;
    public Long count;

    public SearchVenueInGroup(Long groupId, String search, Long count) {
        this.groupId = groupId;
        this.search = search;
        this.count = count;
    }
}
