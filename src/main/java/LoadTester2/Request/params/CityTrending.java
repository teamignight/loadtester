package LoadTester2.Request.params;

import com.sun.org.apache.xpath.internal.operations.Bool;

import java.util.Random;

public class CityTrending {
    public Integer count = 15;
    public Double search_radius = null;
    public Double latitude = null;
    public Double longitude = null;
    public boolean upvote_filter = false;
    public Integer music_filter = null;
    public String search = null;

    public static double latitudeCenter = 41.8020;
    public static double longitudeCenter = -87.6278;
    public static double metersPerMile = 1609.344;

    public CityTrending (boolean random){
        if (random) {
            setupRandomTrendingCall();
        }
    }

    private void setupRandomTrendingCall() {
        Random rand = new Random();
        int type = rand.nextInt(4);
        switch (type) {
            case 0:
                //Regular
                break;
            case 1:
                //Search With Range Parameters
                this.search_radius = search_radius;
                this.latitude = latitudeCenter;
                this.longitude = longitudeCenter;
                break;
            case 2:
                //Upvote filter
                this.upvote_filter = true;
                break;
            case 3:
                //Music Filter
                this.music_filter = rand.nextInt(15);
                break;
        }
    }

    public CityTrending (Double search_radius, Double latitude, Double longitude) {
        this.search_radius = search_radius;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public CityTrending(Boolean upvote_filter) {
        this.upvote_filter = upvote_filter;
    }

    public CityTrending(Integer music_filter) {
        this.music_filter = music_filter;
    }

    public CityTrending(String search) {
        this.search = search;
    }
}
