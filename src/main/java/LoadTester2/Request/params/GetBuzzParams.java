package LoadTester2.Request.params;


public class GetBuzzParams {
    public Long buzzTypeId;
    public Long lastBuzzId = -1l;
    public Long count = 20l;
    public Integer latest = 0;

    public GetBuzzParams(Long buzzTypeId) {
        this.buzzTypeId = buzzTypeId;
    }

    public GetBuzzParams(Long buzzTypeId, Long lastBuzzId) {
        this.buzzTypeId = buzzTypeId;
        this.lastBuzzId = lastBuzzId;
    }

    public GetBuzzParams(Long buzzTypeId, Long lastBuzzId, Long count, boolean latest) {
        this.buzzTypeId = buzzTypeId;
        this.lastBuzzId = lastBuzzId;
        this.count = count;
        this.latest = latest ? 1 : 0;
    }

    @Override
    public String toString() {
        return "GetBuzzParams{" +
                "buzzTypeId=" + buzzTypeId +
                ", lastBuzzId=" + lastBuzzId +
                ", count=" + count +
                ", latest=" + latest +
                '}';
    }
}
