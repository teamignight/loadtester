package LoadTester2.Request.params;

import java.util.Random;

public class Group {
    public Long cityId = 0l;
    public String groupName = null;
    public String groupDescription = null;
    public boolean isPrivate = false;
    public static Random rand = new Random();

    public static Group createRandomGroup() {
        return new Group("GFFGFDGfg" + rand.nextInt(100000), "ONE TWO THREE FOUR", rand.nextBoolean());
    }

    @Override
    public String toString() {
        return "Group{" +
                "cityId=" + cityId +
                ", groupName='" + groupName + '\'' +
                ", groupDescription='" + groupDescription + '\'' +
                ", isPrivate=" + isPrivate +
                '}';
    }

    public Group(String name, String description, boolean isPrivate) {
        this.groupName = name;
        this.groupDescription = description;
        this.isPrivate = isPrivate;
    }
}
