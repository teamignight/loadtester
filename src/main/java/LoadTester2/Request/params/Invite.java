package LoadTester2.Request.params;

public class Invite {
    public Long groupId = null;
    public Long targetUserId = null;

    public Invite(Long groupId, Long targetUserId) {
        this.groupId = groupId;
        this.targetUserId = targetUserId;
    }
}
