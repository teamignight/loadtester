package LoadTester2.Request.params;

import java.util.Random;

public class PostBuzzParams {
    private static String alphabet = "abcdefghijklmnopqrstuvwxyz0123456789";
    private static Random rand = new Random();
    public Long buzzTypeId;
    public String buzzText;

    public PostBuzzParams(Long buzzTypeId, String buzzText) {
        this.buzzTypeId = buzzTypeId;
        this.buzzText = buzzText;
    }

    public PostBuzzParams(Long buzzTypeId) {
        this.buzzTypeId = buzzTypeId;
        int length = rand.nextInt(100) + 5;
        for(int i = 0; i < length; i++){
            this.buzzText += alphabet.charAt(rand.nextInt(35));
        }
    }
}
