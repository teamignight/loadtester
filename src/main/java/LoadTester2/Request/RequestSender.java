package LoadTester2.Request;


import LoadTester2.Request.Type.*;
import LoadTester2.Response.RequestResponse;
import LoadTester2.Response.RequestResponseException;
import LoadTester2.Simulation;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public class RequestSender {
    private static final Logger log = LoggerFactory.getLogger(RequestSender.class);
    private static String destinationUrl = Simulation.connectionUrl;

    public static RequestResponse send(Request request) {
        if (request == null) {
            return null;
        }

        String output = null;
        if (request instanceof GetRequest) {
            output = sendGet(request);
        } else if (request instanceof PostRequest) {
            output = sendPost(request);
        } else if (request instanceof PutRequest) {
            output = sendPut(request);
        } else {
            log.error("Invalid Request Type: {}", request);
            throw new RequestResponseException("Invalid Request Msg: " + request);
        }

        JsonParser jsonParser = new JsonParser();
        JsonObject jsonObject = jsonParser.parse(output).getAsJsonObject();
        return new RequestResponse(request, jsonObject);
    }

    public static String sendPost(Request request)  {
        String postData = null;
        try {
            URL url = new URL(destinationUrl + request.url);

            if (request.msg == null) {
                log.error("MSG CNAT BE NULL: {}", request.msg);
            }
            postData = ((PostMsg)request.msg).getPostData();

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            connection.setRequestProperty("Content-Length", String.valueOf(postData.length()));
            connection.setRequestProperty("X-IGNIGHT-TOKEN", "428CD392-C394-4237-A8B8-E95564333C44");

            // Write data
            OutputStream os = connection.getOutputStream();
            os.write(postData.getBytes());

            // Read response
            StringBuilder responseSB = new StringBuilder();
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String line;
            while ((line = br.readLine()) != null)
                responseSB.append(line);

            // Close streams
            br.close();
            os.close();

            return responseSB.toString();
        } catch (IOException e) {
            log.error("Failed to execute POST uri = {}, data = {}", request.url, postData, e);
            return null;
        }
    }

    public static String sendPut(Request request)  {
        String putData = null;
        try {
            putData = ((PostMsg)request.msg).getPostData();
            URL url = new URL(destinationUrl + request.url);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("PUT");
            connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            connection.setRequestProperty("Content-Length", String.valueOf(putData.length()));
            connection.setRequestProperty("X-IGNIGHT-TOKEN", "428CD392-C394-4237-A8B8-E95564333C44");

            // Write data
            OutputStream os = connection.getOutputStream();
            os.write(putData.getBytes());

            // Read response
            StringBuilder responseSB = new StringBuilder();
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String line;
            while ((line = br.readLine()) != null) {
                responseSB.append(line);
            }

            // Close streams
            br.close();
            os.close();

            return responseSB.toString();
        } catch (IOException e) {
            log.error("Failed to execute PUT uri = {}, data = {}", request.url, putData, e);
            return null;
        }
    }

    public static String sendGet(Request request) {
        log.info("Sending GET: {}", request);
        Map<Object, Object> getData = null;
        try {
            getData = ((GetRequest)request).msg.msg;
            String fullUrl = destinationUrl + request.url + "?";
            for (Map.Entry<Object, Object> entry : getData.entrySet()) {
                Object key = entry.getKey();
                Object value = entry.getValue();
                fullUrl = fullUrl + key + "=" + value + "&";
            }
            fullUrl = StringUtils.left(fullUrl, fullUrl.length() - 1);
            URL url = new URL(fullUrl);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            connection.setRequestProperty("X-IGNIGHT-TOKEN", "428CD392-C394-4237-A8B8-E95564333C44");

            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            return response.toString();
        } catch (IOException e) {
            log.error("Failed to execute GET uri = {}, data = {}", request.url, getData, e);
            return null;
        }
    }
}