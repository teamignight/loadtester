package LoadTester2.Request;

import LoadTester2.Request.Type.GetMsg;
import LoadTester2.Request.Type.PostMsg;
import LoadTester2.Request.Type.GetRequest;
import LoadTester2.Request.Type.PostRequest;
import LoadTester2.Request.Type.PutRequest;
import LoadTester2.Request.params.*;
import LoadTester2.User;
import LoadTester2.Request.Type.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RequestGenerator {
    private static Logger log = LoggerFactory.getLogger(RequestGenerator.class);

    public Request createUser(User user) {
        PostMsg msg = new PostMsg().set("userName", user.username)
                .set("email", user.email)
                .set("password", user.password);
        Request request = new PostRequest(user, "/user", msg);
        log.debug("Sending Request: {}" , request);
        return request;
    }

    public Request setUser(User user) {
        PostMsg msg = new PostMsg().set("userId", user.userId)
                .set("cityId", user.cityId)
                .set("genderId", user.genderId)
                .set("age", user.ageId)
                .set("music", user.music)
                .set("atmosphere", user.atmosphere)
                .set("spendingLimit", user.spendingLimit);
        Request request = new PutRequest(user, "/user/" + user.userId, msg);
        log.debug("Sending Request: {}" , request);
        return request;
    }

    public Request getUser(User user) {
        GetMsg msg = new GetMsg();
        Request request = new GetRequest(user, "/user/" + user.userId, msg);
        log.debug("Sending Request: {}" , request);
        return request;
    }

    public Request updateUser(User user) {
        PostMsg msg = new PostMsg().set("age", user.ageId)
                .set("music", user.music)
                .set("atmosphere", user.atmosphere)
                .set("spendingLimit", user.spendingLimit)
                .set("email", user.email)
                .set("notifyOfGroupInvites", user.notifyOfGroupInvites);
        Request request = new PostRequest(user, "/user/" + user.userId, msg);
        log.debug("Sending Request: {}" , request);
        return request;
    }

    public Request login(User user) {
        PostMsg msg = new PostMsg().set("userName", user.username).set("password", user.password);
        Request request = new PostRequest(user, "/login", msg);
        log.debug("Sending Request: {}" , request);
        return request;
    }

    public Request logout(User user) {
        PostMsg msg = new PostMsg();
        Request request = new PostRequest(user, "/logout/" + user.userId, msg);
        log.debug("Sending Request: {}" , request);
        return request;
    }

    public Request getCityTrending(User user, CityTrending params) {
        GetMsg msg  = new GetMsg().set("count", params.count)
                                  .set("upvote_filter", params.upvote_filter)
                                  .set("music_filter", params.music_filter)
                                  .set("search_radius", params.search_radius)
                                  .set("latitude", params.latitude)
                                  .set("longitude", params.longitude)
                                  .set("search", params.search);
        Request request = new GetRequest(user, "/trending/" + user.userId, msg);
        log.debug("Sending Request: {}" , request);
        return request;
    }

    public Request getUserGroups(User user) {
        GetMsg msg = new GetMsg();
        Request request = new GetRequest(user, "/groups/user/" + user.userId, msg);
        log.debug("Sending Request: {}" , request);
        return request;
    }

    public Request searchForUsersInGroup(User user, String search, long groupId) {
        GetMsg msg = new GetMsg().set("search", search)
                                 .set("groupId", groupId);
        Request request = new GetRequest(user, "/groups/users/search/" + user.userId, msg);
        log.debug("Sending Request: {}" , request);
        return request;
    }

    public Request stopListeningToBuzz(User user) {
        PostMsg msg = new PostMsg();
        Request request=  new PostRequest(user, "/unsubscribe/buzz/" + user.userId, msg);
        log.debug("Sending Request: {}" , request);
        return request;
    }

    public Request createGroup(User user, Group group) {
        PostMsg msg = new PostMsg().set("cityId", group.cityId)
                                   .set("groupName", group.groupName)
                                   .set("groupDescription", group.groupDescription)
                                   .set("isPrivateGroup", group.isPrivate);
        Request request = new PostRequest(user, "/group/add/" + user.userId, msg);
        log.debug("Sending Request: {}" , request);
        return request;
    }

    public Request getGroupInfo(User user, long groupId) {
        GetMsg msg = new GetMsg().set("userId", user.userId);
        Request request = new GetRequest(user, "/group/info/" + groupId, msg);
        log.debug("Sending Request: {}" , request);
        return request;
    }

    public Request getGroupMembers(User user, long groupId) {
        Request request = new GetRequest(user, "/group/members/" + groupId, new GetMsg());
        log.debug("Sending Request: {}" , request);
        return request;
    }

    public Request getGroupTrending(User user, long groupId) {
        GetMsg msg = new GetMsg().set("userId", user.userId);
        Request request = new GetRequest(user, "/group/trending/" + groupId, msg);
        log.debug("Sending Request: {}" , request);
        return request;
    }

    public Request getInbox(User user) {
        Request request = new GetRequest(user, "/inbox/" + user.userId, new GetMsg());
        log.debug("Sending Request: {}" , request);
        return request;
    }

    public Request getPopularGroups(User user, long count) {
        GetMsg msg = new GetMsg().set("count", count);
        Request request = new GetRequest(user, "/group/popular/" + user.userId, msg);
        log.debug("Sending Request: {}" , request);
        return request;
    }

    public Request inviteUser(User user, Invite invite) {
        PostMsg msg = new PostMsg().set("userId", user.userId).set("targetUserId", invite.targetUserId);
        Request request = new PostRequest(user, "/group/invite/" + invite.groupId, msg);
        log.debug("Sending Request: {}" , request);
        return request;
    }

    public Request joinGroup(User user, long groupId) {
        PostMsg msg = new PostMsg().set("userId", user.userId);
        Request request = new PostRequest(user, "/group/join/" + groupId, msg);
        log.debug("Sending Request: {}" , request);
        return request;
    }

    public Request leaveGroup(User user, long groupId) {
        PostMsg msg = new PostMsg().set("userId", user.userId);
        Request request = new PostRequest(user, "/group/leave/" + groupId, msg);
        log.debug("Sending Request: {}" , request);
        return request;
    }

    public Request respondToGroupInvite(User user, long groupId, boolean response) {
        PostMsg msg = new PostMsg().set("userId", user.userId).set("accept", response);
        Request request = new PostRequest(user, "/group/respond/" + groupId, msg);
        log.debug("Sending Request: {}" , request);
        return request;
    }

    public Request searchForGroups(User user, String search) {
        GetMsg msg = new GetMsg().set("search", search);
        Request request = new GetRequest(user, "/group/search/" + user.userId, msg);
        log.debug("Sending Request: {}" , request);
        return request;
    }

    /*
     * TODO: ADD Functions to create post / get images for venues & groups
     */
    public Request addGroupBuzz(User user, PostBuzzParams params) {
        PostMsg msg = new PostMsg().set("userId", user.userId).set("buzzText", params.buzzText);
        Request request = new PostRequest(user, "/buzz/add/group/text/" + params.buzzTypeId, msg);
        log.debug("Sending Request: {}" , request);
        return request;
    }

    public Request getGroupBuzz(User user, GetBuzzParams params) {
        GetMsg msg = new GetMsg().set("userId", user.userId)
                                 .set("lastBuzzId", params.lastBuzzId)
                                 .set("count", params.count)
                                 .set("latest", params.latest);
        Request request = new GetRequest(user, "/buzz/group/" + params.buzzTypeId, msg);
        log.debug("Sending Request: {}" , request);
        return request;
    }

    public Request addVenueBuzz(User user, PostBuzzParams params) {
        PostMsg msg = new PostMsg().set("userId", user.userId).set("buzzText", params.buzzText);
        Request request = new PostRequest(user, "/buzz/add/venue/text/" + params.buzzTypeId, msg);
        log.debug("Sending Request: {}" , request);
        return request;
    }

    public Request getVenueBuzz(User user, GetBuzzParams params) {
        GetMsg msg = new GetMsg().set("userId", user.userId)
                .set("lastBuzzId", params.lastBuzzId)
                .set("count", params.count)
                .set("latest", params.latest);
        Request request = new GetRequest(user, "/buzz/venue/" + params.buzzTypeId, msg);
        log.debug("Sending Request: {}" , request);
        return request;
    }

    public Request getVenueInfo(User user, Long venueId) {
        GetMsg msg = new GetMsg().set("userId", user.userId);
        Request request = new GetRequest(user, "/venue/info/" + venueId, msg);
        log.debug("Sending Request: {}" , request);
        return request;
    }

    public Request setVenueActivity(User user, Long venueId, Long activity) {
        PostMsg msg = new PostMsg().set("userId", user.userId).set("userActivity", activity);
        Request request = new PostRequest(user, "/venue/activity/" + venueId, msg);
        log.debug("Sending Request: {}" , request);
        return request;
    }

    public Request searchVenuesInGroups(User user, SearchVenueInGroup searchVenueInGroup) {
        GetMsg msg = new GetMsg().set("userId", user.userId)
                                 .set("search", searchVenueInGroup.search)
                                 .set("count", searchVenueInGroup.count);
        Request request = new GetRequest(user, "/group/venue/search/" + searchVenueInGroup.groupId, msg);
        log.debug("Sending Request: {}" , request);
        return request;
    }
}
