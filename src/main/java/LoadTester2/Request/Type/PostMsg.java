package LoadTester2.Request.Type;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

public class PostMsg extends RequestMsg {
    public Map<Object, Object> body = new HashMap<Object, Object>();
    private Gson gson = new Gson();

    public PostMsg set(Object key, Object value) {
        if (value != null) {
            msg.put(key, value);
        }
        return this;
    }

    public PostMsg clear() {
        msg.clear();
        return this;
    }

    public String getPostData() {
        body.put("msg", msg);
        return gson.toJson(body).toString();
    }
}
