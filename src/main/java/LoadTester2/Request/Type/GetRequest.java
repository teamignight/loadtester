package LoadTester2.Request.Type;


import LoadTester2.User;

public class GetRequest extends Request {
    public GetRequest(User user, String url, GetMsg msg) {
        this.user = user;
        this.url = url;
        this.msg = msg;
    }
}
