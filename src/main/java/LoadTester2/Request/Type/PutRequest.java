package LoadTester2.Request.Type;

import LoadTester2.User;

public class PutRequest extends Request {
    public PutRequest(User user, String url, PostMsg msg) {
        this.user = user;
        this.url = url;
        this.msg = msg;
    }
}
