package LoadTester2.Request.Type;

import java.util.HashMap;
import java.util.Map;

public class GetMsg extends RequestMsg {
    public GetMsg set(Object key, Object value) {
        if (value != null) {
            msg.put(key, value);
        }
        return this;
    }

    public Map<Object, Object> getParams() {
        return msg;
    }
}
