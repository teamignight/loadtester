package LoadTester2.Request.Type;


import LoadTester2.User;

public abstract class Request {
    public RequestMsg msg;
    public String url;
    public User user;

    @Override
    public String toString() {
        return "Request{" +
                "msg=" + msg +
                ", url='" + url + '\'' +
                ", user=" + user +
                '}';
    }
}
