package LoadTester2;

import LoadTester2.Response.RequestResponse;
import com.google.common.util.concurrent.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IgnightActionCallback implements FutureCallback<RequestResponse> {
    public static ListeningExecutorService executors;
    public static final Logger logger = LoggerFactory.getLogger(IgnightActionCallback.class);

    public IgnightActionCallback(ListeningExecutorService executors) {
        this.executors = executors;
    }

    @Override
    public void onSuccess(RequestResponse requestResponse) {
        if (requestResponse != null) {
            if (requestResponse.success && requestResponse.nextAction != null) {
                ListenableFuture<RequestResponse> future = executors.submit(requestResponse.nextAction);
                Futures.addCallback(future, new IgnightActionCallback(executors));
            }
        }
    }

    @Override
    public void onFailure(Throwable throwable) {
        logger.error("Failed to Run Action: {}", throwable.getMessage(), throwable);
    }
}
