package LoadTester2.actions;

import LoadTester2.Request.Type.Request;
import LoadTester2.Request.params.CityTrending;
import LoadTester2.Response.RequestResponse;
import LoadTester2.User;
import com.google.gson.JsonElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.concurrent.Callable;

public class GetTrending extends IgnightAction {
    private static final Logger log = LoggerFactory.getLogger(GetTrending.class);
    CityTrending params;

    public GetTrending(User user, CityTrending cityTrending) {
        super(user);
        this.params = cityTrending;
    }

    @Override
    public Request doAction() {
        return rG.getCityTrending(user, params);
    }

    @Override
    public Callable<RequestResponse> getNextAction(RequestResponse response) {
        if (response.success) {
            ArrayList<ActionWeight> actions = new ArrayList<ActionWeight>();
            ArrayList<TrendingVenue> trendingVenues = parseResponse(response.body);
            if (trendingVenues.size() > 0) {
                TrendingVenue randomVenue = getRandomVenue(trendingVenues);
                Long randomActivity = getRandomgActivityOnVenue(randomVenue);
                actions.add(new ActionWeight(new SetVenueActivity(user, randomActivity), 20));
                actions.add(new ActionWeight(new GetUserGroups(user), 20));
                actions.add(new ActionWeight(new GetTrending(user, new CityTrending(true)), 20));
                actions.add(new ActionWeight(new GetVenueInfo(user, randomVenue.venueId), 20));
                actions.add(new ActionWeight(new Logout(user), 10));
                actions.add(new ActionWeight(new Exit(user), 10));
            } else {
                actions.add(new ActionWeight(new GetTrending(user, new CityTrending(false)), 70));
                actions.add(new ActionWeight(new GetUserGroups(user), 20));
                actions.add(new ActionWeight(new Logout(user), 10));
            }
            return getRandomAction(actions);
        } else {
            log.error("Failed to get trending list for user: {}, response: {}", user, response.body);
            return null;
        }
    }

    private ArrayList<TrendingVenue> parseResponse(JsonElement element) {
        ArrayList<TrendingVenue> trendingVenues = new ArrayList<TrendingVenue>();
        if (element.isJsonArray()) {
            for (JsonElement elem : element.getAsJsonArray()) {
                trendingVenues.add(gson.fromJson(elem, TrendingVenue.class));
            }
        }
        return trendingVenues;
    }

    private Long getRandomgActivityOnVenue(TrendingVenue trendingVenue) {
        log.info("RandomActivity: venueId: {}, TV: {}", trendingVenue.venueId, trendingVenue);
        int userInput = trendingVenue.userInput.intValue();
        while (userInput == trendingVenue.userInput.intValue()) {
            userInput = rand.nextInt(3) - 1;
        }
        return Long.valueOf(userInput);
    }

    private TrendingVenue getRandomVenue(ArrayList<TrendingVenue> trendingVenues) {
        if (trendingVenues.size() > 0) {
            return trendingVenues.get(rand.nextInt(trendingVenues.size()));
        }
        log.error("Got Trending Venue Size of 0");
        return null;
    }

    public static class TrendingVenue {
        public Long venueId;
        public String venueName;
        public Long userBarColor;
        public Long userInput;
        public Double longitude;
        public Double latitude;

        @Override
        public String toString() {
            return "TrendingVenue{" +
                    "venueId=" + venueId +
                    ", venueName='" + venueName + '\'' +
                    ", userBarColor=" + userBarColor +
                    ", userInput=" + userInput +
                    ", longitude=" + longitude +
                    ", latitude=" + latitude +
                    '}';
        }
    }
}
