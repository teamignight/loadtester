package LoadTester2.actions;


import LoadTester2.Request.Type.Request;
import LoadTester2.Request.params.CityTrending;
import LoadTester2.Request.params.GetBuzzParams;
import LoadTester2.Request.params.PostBuzzParams;
import LoadTester2.Response.RequestResponse;
import LoadTester2.User;
import junit.framework.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Callable;

public class AddVenueBuzz extends IgnightAction {
    private static final Logger log = LoggerFactory.getLogger(AddVenueBuzz.class);

    public PostBuzzParams params;
    public AddVenueBuzz(User user, PostBuzzParams params) {
        super(user);
        this.params = params;
    }

    @Override
    public Request doAction() {
        return rG.addVenueBuzz(user, params);
    }

    @Override
    public Callable<RequestResponse> getNextAction(RequestResponse response) {
        if (response.success) {
            if (response.body != null && response.body.isJsonArray() && response.body.getAsJsonArray().size() > 0) {
                AddVenueBuzzResponse addVenueBuzzResponse = gson.fromJson(response.body, AddVenueBuzzResponse.class);
                addVenueBuzzResponse.validate(params);
            }

            ArrayList<ActionWeight> actions = new ArrayList<ActionWeight>();
            actions.add(new ActionWeight(new AddVenueBuzz(user, new PostBuzzParams(params.buzzTypeId)), 20));
            actions.add(new ActionWeight(new GetVenueBuzz(user, new GetBuzzParams(params.buzzTypeId)), 20));
            actions.add(new ActionWeight(new GetVenueInfo(user, params.buzzTypeId), 20));
            actions.add(new ActionWeight(new GetTrending(user, new CityTrending(true)), 29));
            actions.add(new ActionWeight(new SetVenueActivity(user, new Random().nextInt(3) - 1), 10));
            actions.add(new ActionWeight(new Exit(user), 1));
            return getRandomAction(actions);
        } else {
            log.error("Failed to add buzz to venue: {}, reason: {}", params.buzzTypeId, response.reason);
            return null;
        }
    }

    public static class AddVenueBuzzResponse {
        public long buzzId;
        public String buzz;

        public void validate(PostBuzzParams params) {
            Assert.assertEquals(params.buzzText, buzz);
        }
    }
}
