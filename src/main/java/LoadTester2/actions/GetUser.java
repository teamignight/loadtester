package LoadTester2.actions;

import LoadTester.DataCollector;
import LoadTester2.Request.RequestSender;
import LoadTester2.Request.Type.Request;
import LoadTester2.Request.params.CityTrending;
import LoadTester2.Response.RequestResponse;
import LoadTester2.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Callable;

public class GetUser extends IgnightAction {
    static final Logger log = LoggerFactory.getLogger(GetUser.class);
    public GetUser(User user) {
        super(user);
    }

    @Override
    public Request doAction() {
        return rG.getUser(user);
    }

    @Override
    public Callable<RequestResponse> getNextAction(RequestResponse response) {
        if (response.success) {
            Login.ReturnedUser returnedUser = gson.fromJson(response.body, Login.ReturnedUser.class);
            DataCollector.addNewUser(user);
            return new GetTrending(user, new CityTrending(false));
        }

        log.error("Failed to get info for user: {}, response: {}", user, response.body);
        return null;
    }
}
