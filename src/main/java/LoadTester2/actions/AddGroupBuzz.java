package LoadTester2.actions;

import LoadTester.Utils;
import LoadTester2.Request.Type.Request;
import LoadTester2.Request.params.GetBuzzParams;
import LoadTester2.Request.params.PostBuzzParams;
import LoadTester2.Response.RequestResponse;
import LoadTester2.User;
import junit.framework.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.concurrent.Callable;

public class AddGroupBuzz extends IgnightAction {
    private static final Logger log = LoggerFactory.getLogger(AddGroupBuzz.class);

    public PostBuzzParams params;

    public AddGroupBuzz(User user, PostBuzzParams params) {
        super(user);
        this.params = params;
    }

    @Override
    public Request doAction() {
        return rG.addGroupBuzz(user, params);
    }

    @Override
    public Callable<RequestResponse> getNextAction(RequestResponse response) {
        if (response.success) {
            AddGroupBuzzResponse addGroupBuzzResponse = gson.fromJson(response.body, AddGroupBuzzResponse.class);
            addGroupBuzzResponse.validate(params);

            ArrayList<ActionWeight> actions = new ArrayList<ActionWeight>();
            actions.add(new ActionWeight(new AddGroupBuzz(user, new PostBuzzParams(params.buzzTypeId)), 20));
            actions.add(new ActionWeight(new GetGroupBuzz(user, new GetBuzzParams(params.buzzTypeId)), 25));
            actions.add(new ActionWeight(new GetGroupTrending(user, params.buzzTypeId), 10));
            actions.add(new ActionWeight(new GetGroupMembers(user, params.buzzTypeId), 10));
            actions.add(new ActionWeight(new GetGroupInfo(user, params.buzzTypeId), 5));
            actions.add(new ActionWeight(new GetUserGroups(user), 5));
            actions.add(new ActionWeight(new GetPopularGroups(user), 5));
            actions.add(new ActionWeight(new GetInbox(user), 5));
            actions.add(new ActionWeight(new SearchForGroups(user), 14));
            actions.add(new ActionWeight(new Exit(user), 1));
            return getRandomAction(actions);
        } else {
            log.error("Failed to add buzz to group: {}, reason: {}", params.buzzTypeId, response.reason);
            return null;
        }
    }

    public static class AddGroupBuzzResponse {
        public long buzzId;
        public String buzz;

        public void validate(PostBuzzParams params) {
            Assert.assertEquals(params.buzzText, buzz);
        }
    }
}
