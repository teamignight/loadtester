package LoadTester2.actions;

import LoadTester2.Request.Type.Request;
import LoadTester2.Response.RequestResponse;
import LoadTester2.User;

import java.util.concurrent.Callable;

/**
 * Created by AChordia on 9/3/14.
 */
public class RespondToGroupInvite extends IgnightAction {
    public long groupId;
    public boolean response;

    public RespondToGroupInvite(User user, long groupId, boolean response) {
        super(user);
        this.groupId = groupId;
        this.response = response;
    }

    @Override
    public Request doAction() {
        return rG.respondToGroupInvite(user, groupId, response);
    }

    @Override
    public Callable<RequestResponse> getNextAction(RequestResponse response) {
        return null;
    }
}
