package LoadTester2.actions;

import LoadTester2.Request.Type.Request;
import LoadTester2.Request.params.CityTrending;
import LoadTester2.Request.params.GetBuzzParams;
import LoadTester2.Response.RequestResponse;
import LoadTester2.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Callable;

public class GetVenueInfo extends IgnightAction {
    private static final Logger log = LoggerFactory.getLogger(GetVenueInfo.class);
    public long venueId;

    public GetVenueInfo(User user, long venueId) {
        super(user);
        this.venueId = venueId;
    }

    @Override
    public Request doAction() {
        return rG.getVenueInfo(user, venueId);
    }

    @Override
    public Callable<RequestResponse> getNextAction(RequestResponse response) {
        if (response.success) {
            ArrayList<ActionWeight> actions = new ArrayList<ActionWeight>();
            actions.add(new ActionWeight(new GetVenueBuzz(user, new GetBuzzParams(venueId)), 20));
            actions.add(new ActionWeight(new GetVenueInfo(user, venueId), 20));
            actions.add(new ActionWeight(new GetTrending(user, new CityTrending(true)), 20));
            actions.add(new ActionWeight(new SetVenueActivity(user, new Random().nextInt(3) - 1), 10));
            actions.add(new ActionWeight(new Exit(user), 10));
            return getRandomAction(actions);
        } else {
            log.error("Failed to get venue info for venue: {}, user: {}, reason: {}", venueId, user, response.reason);
            return null;
        }
    }
}
