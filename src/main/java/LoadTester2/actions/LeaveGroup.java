package LoadTester2.actions;

import LoadTester2.Request.Type.Request;
import LoadTester2.Response.RequestResponse;
import LoadTester2.User;
import junit.framework.TestCase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Callable;

import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.*;

public class LeaveGroup extends IgnightAction {
    private static final Logger log = LoggerFactory.getLogger(LeaveGroup.class);
    public long groupId;

    public LeaveGroup(User user, long groupId) {
        super(user);
        this.groupId = groupId;
    }

    @Override
    public Request doAction() {
        return rG.leaveGroup(user, groupId);
    }

    @Override
    public Callable<RequestResponse> getNextAction(RequestResponse response) {
        if (response.success) {
            LeaveGroupResponse leaveGroupResponse = gson.fromJson(response.body, LeaveGroupResponse.class);
            leaveGroupResponse.validate(user, groupId);
            user.userGroups.remove(groupId);
            return new GetUserGroups(user);
        }
        log.error("Failed to leave group: {}, user: {}, reason: {}", groupId, user, response.reason);
        return null;
    }

    public static class LeaveGroupResponse {
        public Long groupId;

        public void validate(User user, Long groupId) {
            assertEquals(groupId, this.groupId);
            assertTrue(user.userGroups.containsKey(groupId));
        }
    }
}
