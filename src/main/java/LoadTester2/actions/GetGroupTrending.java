package LoadTester2.actions;

import LoadTester2.Request.Type.Request;
import LoadTester2.Request.params.GetBuzzParams;
import LoadTester2.Response.RequestResponse;
import LoadTester2.User;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class GetGroupTrending extends IgnightAction {
    private static final Logger log = LoggerFactory.getLogger(GetGroupTrending.class);
    long groupId;

    public GetGroupTrending(User user, long groupId) {
        super(user);
        this.groupId = groupId;
    }

    @Override
    public Request doAction() {
        return rG.getGroupTrending(user, groupId);
    }

    @Override
    public Callable<RequestResponse> getNextAction(RequestResponse response) {
        if (response.success) {
            List<GroupTrending> groupTrending = parseResponse(response.body);

            ArrayList<ActionWeight> actions = new ArrayList<ActionWeight>();
            actions.add(new ActionWeight(new GetGroupBuzz(user, new GetBuzzParams(groupId)), 30));
            actions.add(new ActionWeight(new GetGroupMembers(user, groupId), 30));
            if (groupTrending.size() > 0) {
                actions.add(new ActionWeight(new SetVenueActivity(user, rand.nextInt(3) - 1), 10));
                actions.add(new ActionWeight(new Logout(user), 20));
            } else {
                actions.add(new ActionWeight(new Logout(user), 30));
            }
            actions.add(new ActionWeight(new Exit(user), 10));
            return getRandomAction(actions);
        } else {
            log.error("Failed to get group trending for group: {}, user: {}", groupId, user);
            return null;
        }
    }

    private List<GroupTrending> parseResponse(JsonElement element) {
        List<GroupTrending> trending = new ArrayList<GroupTrending>();
        if (element.isJsonArray()) {
            JsonArray array = element.getAsJsonArray();
            for (JsonElement obj : array) {
                trending.add(gson.fromJson(obj, GroupTrending.class));
            }
        }
        return trending;
    }

    public static class GroupTrending {
        public Long venueId;
        public Long userVenueValue;
        public String venueName;
        public Long userInput;
        public Double longitude;
        public Double latitude;
    }
}
