package LoadTester2.actions;

import LoadTester2.Request.Type.Request;
import LoadTester2.Response.RequestResponse;
import LoadTester2.User;

import java.util.concurrent.Callable;

public class JoinGroup extends IgnightAction {
    long groupId;

    public JoinGroup(User user, long groupId) {
        super(user);
        this.groupId = groupId;
    }

    @Override
    public Request doAction() {
        return rG.joinGroup(user, groupId);
    }

    @Override
    public Callable<RequestResponse> getNextAction(RequestResponse response) {
        return null;
    }
}
