package LoadTester2.actions;

import LoadTester2.Request.Type.Request;
import LoadTester2.Request.params.GetBuzzParams;
import LoadTester2.Request.params.PostBuzzParams;
import LoadTester2.Response.RequestResponse;
import LoadTester2.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.concurrent.Callable;

public class GetGroupBuzz extends IgnightAction {
    static final Logger log = LoggerFactory.getLogger(GetGroupBuzz.class);
    public GetBuzzParams params;

    public GetGroupBuzz(User user, GetBuzzParams params) {
        super(user);
        this.params = params;
    }

    @Override
    public Request doAction() {
        return rG.getGroupBuzz(user, params);
    }

    @Override
    public Callable<RequestResponse> getNextAction(RequestResponse response) {
        if (response.success) {
            GetBuzzResponse getBuzzResponse = gson.fromJson(response.body, GetBuzzResponse.class);

            ArrayList<ActionWeight> actions = new ArrayList<ActionWeight>();
            actions.add(new ActionWeight(new AddGroupBuzz(user, new PostBuzzParams(params.buzzTypeId)), 40));
            actions.add(new ActionWeight(new GetGroupBuzz(user, new GetBuzzParams(params.buzzTypeId)), 5));
            actions.add(new ActionWeight(new GetGroupTrending(user, params.buzzTypeId), 10));
            actions.add(new ActionWeight(new GetGroupMembers(user, params.buzzTypeId), 10));
            actions.add(new ActionWeight(new GetGroupInfo(user, params.buzzTypeId), 5));
            actions.add(new ActionWeight(new GetUserGroups(user), 5));
            actions.add(new ActionWeight(new GetPopularGroups(user), 5));
            actions.add(new ActionWeight(new GetInbox(user), 5));
            actions.add(new ActionWeight(new SearchForGroups(user), 14));
            actions.add(new ActionWeight(new Exit(user), 1));
            return getRandomAction(actions);
        } else {
            log.error("Failed to get group buzz with params: {}", params);
            return null;
        }
    }

    public static class GetBuzzResponse {
        public ArrayList<Buzz> buzz = new ArrayList<Buzz>();

        public static class Buzz {
            public long buzzId;
            public long userId;
            public BuzzType buzzType;
            public String buzzText;
            public long buzzDateStamp;
            public String userName;
            public String chatPictureUrl;
        }

        public static enum BuzzType {
            TEXT, IMAGE
        }
    }
}
