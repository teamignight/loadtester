package LoadTester2.actions;

import LoadTester2.Request.Type.Request;
import LoadTester2.Request.params.CityTrending;
import LoadTester2.Response.RequestResponse;
import LoadTester2.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Callable;

public class Exit extends IgnightAction {
    private static final Logger log = LoggerFactory.getLogger(Exit.class);

    public Exit(User user) {
        super(user);
    }

    @Override
    public Request doAction() {
        log.info("Exiting: {}", user);
        return rG.getUser(user);
    }

    @Override
    public Callable<RequestResponse> getNextAction(RequestResponse response) {
//        return null;
        return new Login(user);
//        return new GetTrending(user, new CityTrending(true));
    }
}
