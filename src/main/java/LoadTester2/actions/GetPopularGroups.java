package LoadTester2.actions;

import LoadTester.Utils;
import LoadTester2.Request.Type.Request;
import LoadTester2.Response.RequestResponse;
import LoadTester2.User;
import com.google.gson.JsonElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class GetPopularGroups extends IgnightAction {
    private static final Logger log = LoggerFactory.getLogger(GetPopularGroups.class);

    long count = 15;
    public GetPopularGroups(User user) {
        super(user);
    }

    public GetPopularGroups(User user, long count) {
        super(user);
        if (count > 0) {
            this.count = count;
        }
    }

    @Override
    public Request doAction() {
        return rG.getPopularGroups(user, count);
    }

    @Override
    public Callable<RequestResponse> getNextAction(RequestResponse response) {
        if (response.success) {
            Long randomGroupId = getRandomPopularGroupId(parseResponse(response.body));
            ArrayList<ActionWeight> actions = new ArrayList<ActionWeight>();
            if (randomGroupId != null) {
                actions.add(new ActionWeight(new GetGroupInfo(user, randomGroupId), 30));
                actions.add(new ActionWeight(new GetUserGroups(user), 20));
                actions.add(new ActionWeight(new SearchForGroups(user, Utils.getRandomName(rand.nextInt(5))), 20));
                actions.add(new ActionWeight(new GetInbox(user), 29));
            } else {
                actions.add(new ActionWeight(new GetUserGroups(user), 30));
                actions.add(new ActionWeight(new SearchForGroups(user, Utils.getRandomName(rand.nextInt(5))), 30));
                actions.add(new ActionWeight(new GetInbox(user), 30));
            }
            actions.add(new ActionWeight(new Exit(user), 1));
            return getRandomAction(actions);
        } else {
            log.error("Failed to get popular groups response: {}. reason: {}", user, response.reason);
            return null;
        }
    }

    private ArrayList<PopularGroup> parseResponse(JsonElement body) {
        ArrayList<PopularGroup> popularGroups = new ArrayList<PopularGroup>();
        if (body.isJsonArray()) {
            for (JsonElement group : body.getAsJsonArray()) {
                popularGroups.add(gson.fromJson(group, PopularGroup.class));
            }
        }
        return popularGroups;
    }

    private Long getRandomPopularGroupId(List<PopularGroup> popularGroups) {
        if (popularGroups.size() > 0) {
            return popularGroups.get(rand.nextInt(popularGroups.size())).groupId;
        }
        return null;
    }

    public static class PopularGroup {
        public Long groupId;
        public String groupName;
        public Long noOfMembers;
        public boolean isMember;
    }
}
