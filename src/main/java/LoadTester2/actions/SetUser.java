package LoadTester2.actions;

import LoadTester.DataCollector;
import LoadTester2.Request.Type.Request;
import LoadTester2.Request.params.CityTrending;
import LoadTester2.Response.RequestResponse;
import LoadTester2.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Callable;

public class SetUser extends IgnightAction{
    static final Logger log = LoggerFactory.getLogger(SetUser.class);
    public SetUser(User user) {
        super(user);
    }

    @Override
    public Request doAction() {
        return rG.setUser(user);
    }

    @Override
    public Callable<RequestResponse> getNextAction(RequestResponse response) {
        if (response.success) {
            log.debug("Successfully set user's DNA: {}", user);
            DataCollector.addNewUser(user);
            return new GetTrending(user, new CityTrending(false));
        }
        log.error("Failed to set user: {} dna, response: {}", response.body);
        return null;
    }
}
