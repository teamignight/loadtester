package LoadTester2.actions;

import LoadTester2.Request.Type.Request;
import LoadTester2.Response.RequestResponse;
import LoadTester2.User;

import java.util.concurrent.Callable;

public class Logout extends IgnightAction {
    public Logout(User user) {
        super(user);
    }

    @Override
    public Request doAction() {
        return rG.logout(user);
    }

    @Override
    public Callable<RequestResponse> getNextAction(RequestResponse response) {
        return new Login(user);
    }
}
