package LoadTester2.actions;

import LoadTester2.Request.Type.Request;
import LoadTester2.Request.params.CityTrending;
import LoadTester2.Response.RequestResponse;
import LoadTester2.User;

import java.util.concurrent.Callable;

public class SearchForVenues extends IgnightAction {
    public String search;

    public SearchForVenues(User user, String search) {
        super(user);
        this.search = search;
    }

    @Override
    public Request doAction() {
        return rG.getCityTrending(user, new CityTrending(search));
    }

    @Override
    public Callable<RequestResponse> getNextAction(RequestResponse response) {
        return null;
    }
}
