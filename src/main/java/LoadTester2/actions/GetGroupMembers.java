package LoadTester2.actions;

import LoadTester.Utils;
import LoadTester2.Request.Type.Request;
import LoadTester2.Request.params.GetBuzzParams;
import LoadTester2.Response.RequestResponse;
import LoadTester2.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.concurrent.Callable;

public class GetGroupMembers extends IgnightAction {
    private static final Logger log = LoggerFactory.getLogger(GetGroupMembers.class);
    public long groupId;

    public GetGroupMembers(User user, long groupId) {
        super(user);
        this.groupId = groupId;
    }

    @Override
    public Request doAction() {
        return rG.getGroupMembers(user, groupId);
    }

    @Override
    public Callable<RequestResponse> getNextAction(RequestResponse response) {
        if (response.success) {
            GetGroupMembersResponse getGroupMembersResponse = gson.fromJson(response.body, GetGroupMembersResponse.class);
            ArrayList<ActionWeight> actions = new ArrayList<ActionWeight>();
            actions.add(new ActionWeight(new GetGroupBuzz(user, new GetBuzzParams(groupId)), 20));
            actions.add(new ActionWeight(new SearchForUsers(user, Utils.getRandomName(3l), groupId), 30));
            actions.add(new ActionWeight(new GetGroupTrending(user, groupId), 40));
            actions.add(new ActionWeight(new Exit(user), 10));
            return getRandomAction(actions);
        } else {
            log.error("Failed to get group members for group: {}, user: {}", groupId, user);
            return null;
        }
    }

    public static class GetGroupMembersResponse {
        public long adminId;
        public ArrayList<Member> groupMembers;

        public static class Member {
            public long userId;
            public String userName;
            public String chatPictureUrl;
        }
    }
}
