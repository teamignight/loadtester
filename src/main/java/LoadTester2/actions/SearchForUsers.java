package LoadTester2.actions;

import LoadTester.Utils;
import LoadTester2.Request.Type.Request;
import LoadTester2.Request.params.Invite;
import LoadTester2.Request.params.PostBuzzParams;
import LoadTester2.Response.RequestResponse;
import LoadTester2.User;
import com.google.gson.JsonElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class SearchForUsers extends IgnightAction {
    private static final Logger log = LoggerFactory.getLogger(SearchedUser.class);
    public String search;
    public long groupId;

    public SearchForUsers(User user, String search, long groupId) {
        super(user);
        this.search = search;
        this.groupId = groupId;
    }

    @Override
    public Request doAction() {
        return rG.searchForUsersInGroup(user, search, groupId);
    }

    @Override
    public Callable<RequestResponse> getNextAction(RequestResponse response) {
        if (response.success) {
            List<SearchedUser> users = parseMessage(response.body);
            List<ActionWeight> actions = new ArrayList<ActionWeight>();
            actions.add(new ActionWeight(new GetGroupTrending(user, groupId), 20));
            if (users.size() > 0) {
                actions.add(new ActionWeight(new InviteUser(user, new Invite(groupId, users.get(rand.nextInt(users.size())).userId)), 40));
            } else {
                actions.add(new ActionWeight(new SearchForUsers(user, Utils.getRandomName(3), groupId), 40));
            }
            actions.add(new ActionWeight(new GetGroupInfo(user, groupId), 20));
            actions.add(new ActionWeight(new AddGroupBuzz(user, new PostBuzzParams(groupId)), 20));
            return getRandomAction(actions);
        } else {
            log.error("Failed to get searched users, request: {}, response: {}", response.requestMsg, response.body);
        }
        return null;
    }

    private static List<SearchedUser> parseMessage(JsonElement element) {
        List<SearchedUser> users = new ArrayList<SearchedUser>();
        if (element.isJsonArray()) {
            for(JsonElement elem : element.getAsJsonArray()) {
                users.add(gson.fromJson(elem, SearchedUser.class));
            }
        }
        return users;
    }

    public static class SearchedUser {
        public Long userId;
        public String userName;
        public GroupStatus groupStatus;
    }

    public static enum GroupStatus {
        NON_MEMBER,
        PENDING_MEMBER,
        MEMBER
    }
}
