package LoadTester2.actions;

import LoadTester2.Request.Type.Request;
import LoadTester2.Request.params.CityTrending;
import LoadTester2.Response.RequestResponse;
import LoadTester2.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class Login extends IgnightAction {
    private final static Logger log = LoggerFactory.getLogger(Login.class);
    public Login(User user) {
        super(user);
    }

    @Override
    public Request doAction() {
        return rG.login(user);
    }

    @Override
    public Callable<RequestResponse> getNextAction(RequestResponse response) {
        if (response.success) {
            if (response.body != null) {
                ReturnedUser returnedUser = gson.fromJson(response.body, ReturnedUser.class);
                if (!returnedUser.isSameUser(user)) {
                    log.error("User: {}, Returned User: {}, body: {}", user, returnedUser, response.body);
                    return null;
                }
                return new GetTrending(user, new CityTrending(false));
            }
            return null;
        }
        log.error("Failed to login for user: {}", user);
        return null;
    }

    public static class ReturnedUser {
        public long id;
        public boolean userInfoSet;
        public boolean pushSet;
        public long cityId;
        public String userName;
        public String profilePictureURL;
        public long genderId;
        public String email;
        public boolean notifyOfGroupInvites;

        public long age;
        public ArrayList<Long> music;
        public ArrayList<Long> atmosphere;
        public long spending;
        public boolean dnaSet;

        @Override
        public String toString() {
            return "ReturnedUser{" +
                    "id=" + id +
                    ", userInfoSet=" + userInfoSet +
                    ", pushSet=" + pushSet +
                    ", cityId=" + cityId +
                    ", userName='" + userName + '\'' +
                    ", profilePictureURL='" + profilePictureURL + '\'' +
                    ", genderId=" + genderId +
                    ", email='" + email + '\'' +
                    ", notifyOfGroupInvites=" + notifyOfGroupInvites +
                    ", age=" + age +
                    ", music=" + music +
                    ", atmosphere=" + atmosphere +
                    ", spending=" + spending +
                    ", dnaSet=" + dnaSet +
                    '}';
        }

        public boolean isSameUser(User user) {
            return this.id == user.userId &&
                    this.cityId == user.cityId &&
                    this.userName.equals(user.username) &&
                    this.genderId == user.genderId &&
                    this.email.equals(user.email) &&
                    this.notifyOfGroupInvites == user.notifyOfGroupInvites &&
                    this.age == user.ageId &&
                    this.spending == user.spendingLimit &&
                    comparePreferences(this.music, user.music) &&
                    comparePreferences(this.atmosphere, user.atmosphere);
        }

        private static boolean comparePreferences(List<Long> one, List<Long> two) {
            if (one == null || two == null) {
                return false;
            }

            if (one.size() != two.size()) {
                return false;
            }

            for (int i = 0; i < one.size(); i++) {
                if (! one.get(i).equals(two.get(i))) {
                    return false;
                }
            }
            return true;
        }
    }
}
