package LoadTester2.actions;

import LoadTester2.Request.Type.Request;
import LoadTester2.Request.params.CityTrending;
import LoadTester2.Response.RequestResponse;
import LoadTester2.User;
import com.google.gson.JsonElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class GetInbox extends IgnightAction {
    private static final Logger log = LoggerFactory.getLogger(GetInbox.class);

    public GetInbox(User user) {
        super(user);
    }

    @Override
    public Request doAction() {
        return rG.getInbox(user);
    }

    @Override
    public Callable<RequestResponse> getNextAction(RequestResponse response) {
        if (response.success) {
            List<InboxInvite> invites = parseInvites(response.body);
            ArrayList<ActionWeight> actions = new ArrayList<ActionWeight>();
            if (invites.size() > 0) {
                long randomGroup = invites.get(rand.nextInt(invites.size())).groupId;
                boolean accept = rand.nextBoolean();
                actions.add(new ActionWeight(new RespondToGroupInvite(user, randomGroup, accept), 30));
                actions.add(new ActionWeight(new GetGroupInfo(user, randomGroup), 15));
                actions.add(new ActionWeight(new GetPopularGroups(user), 10));
                actions.add(new ActionWeight(new GetUserGroups(user), 10));
                actions.add(new ActionWeight(new GetTrending(user, new CityTrending(true)), 34));
            } else {
                actions.add(new ActionWeight(new GetPopularGroups(user), 30));
                actions.add(new ActionWeight(new GetUserGroups(user), 30));
                actions.add(new ActionWeight(new GetTrending(user, new CityTrending(true)), 39));
            }
            actions.add(new ActionWeight(new Exit(user), 1));
            return getRandomAction(actions);
        } else {
            log.error("Failed to get inbox msgs for user: {}", user);
            return null;
        }
    }

    private List<InboxInvite> parseInvites(JsonElement element) {
        List<InboxInvite> invites = new ArrayList<InboxInvite>();
        if (element.isJsonArray()) {
            for (JsonElement obj : element.getAsJsonArray()) {
                invites.add(gson.fromJson(obj, InboxInvite.class));
            }
        }
        return invites;
    }

    public static class InboxInvite {
        public Long groupId;
        public String groupName;
        public Long timestamp;
        public String userName;
        public String userPicture;
    }
}
