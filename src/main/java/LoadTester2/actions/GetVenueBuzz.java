package LoadTester2.actions;

import LoadTester2.Request.Type.Request;
import LoadTester2.Request.params.CityTrending;
import LoadTester2.Request.params.GetBuzzParams;
import LoadTester2.Request.params.PostBuzzParams;
import LoadTester2.Response.RequestResponse;
import LoadTester2.User;
import com.google.gson.JsonElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Callable;

public class GetVenueBuzz extends IgnightAction {
    static final Logger log = LoggerFactory.getLogger(GetGroupBuzz.class);

    public GetBuzzParams params;
    public GetVenueBuzz(User user, GetBuzzParams params) {
        super(user);
        this.params = params;
    }

    @Override
    public Request doAction() {
        return rG.getVenueBuzz(user, params);
    }

    @Override
    public Callable<RequestResponse> getNextAction(RequestResponse response) {
        if (response.success) {
            GetGroupBuzz.GetBuzzResponse getBuzzResponse = parseRequest(response.body);
            ArrayList<ActionWeight> actions = new ArrayList<ActionWeight>();
            actions.add(new ActionWeight(new AddVenueBuzz(user, new PostBuzzParams(params.buzzTypeId)), 40));
            actions.add(new ActionWeight(new GetVenueBuzz(user, new GetBuzzParams(params.buzzTypeId)), 10));
            actions.add(new ActionWeight(new GetVenueInfo(user, params.buzzTypeId), 20));
            actions.add(new ActionWeight(new GetTrending(user, new CityTrending(true)), 10));
            actions.add(new ActionWeight(new SetVenueActivity(user, new Random().nextInt(3)), 10));
            actions.add(new ActionWeight(new Exit(user), 10));
            return getRandomAction(actions);
        } else {
            log.error("Failed to get Venue Buzz with params: {}, reason: {}", params, response.reason);
            return null;
        }
    }

    public static GetGroupBuzz.GetBuzzResponse parseRequest(JsonElement element) {
        GetGroupBuzz.GetBuzzResponse response = new GetGroupBuzz.GetBuzzResponse();
        if (element.isJsonArray()) {
            for (JsonElement elem : element.getAsJsonArray()) {
                GetGroupBuzz.GetBuzzResponse.Buzz buzz = gson.fromJson(elem, GetGroupBuzz.GetBuzzResponse.Buzz.class);
                response.buzz.add(buzz);
            }
        }
        return response;
    }

}
