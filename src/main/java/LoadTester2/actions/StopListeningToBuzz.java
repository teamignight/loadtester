package LoadTester2.actions;

import LoadTester2.Request.Type.Request;
import LoadTester2.Response.RequestResponse;
import LoadTester2.User;

import java.util.concurrent.Callable;

public class StopListeningToBuzz extends IgnightAction {
    public StopListeningToBuzz(User user) {
        super(user);
    }

    @Override
    public Request doAction() {
        return rG.stopListeningToBuzz(user);
    }

    @Override
    public Callable<RequestResponse> getNextAction(RequestResponse response) {
        return null;
    }
}
