package LoadTester2.actions;

import LoadTester2.Request.Type.Request;
import LoadTester2.Response.RequestResponse;
import LoadTester2.User;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.concurrent.Callable;

public class GetGroupInfo extends IgnightAction {
    private static final Logger log = LoggerFactory.getLogger(GetGroupInfo.class);
    private Gson gson = new Gson();

    public long groupId;

    public GetGroupInfo(User user, Long groupId) {
        super(user);
        this.groupId = groupId;
    }

    @Override
    public Request doAction() {
        return rG.getGroupInfo(user, groupId);
    }

    @Override
    public Callable<RequestResponse> getNextAction(RequestResponse response) {
        if (response.success) {
            GroupInfo groupInfo = gson.fromJson(response.body, GroupInfo.class);

            ArrayList<ActionWeight> actions =  new ArrayList<ActionWeight>();
            if (user.userGroups.get(groupInfo.id) != null) {   //User member of group
                actions.add(new ActionWeight(new LeaveGroup(user, groupInfo.id), 45));
                actions.add(new ActionWeight(new GetGroupTrending(user, groupInfo.id), 54));
                actions.add(new ActionWeight(new Exit(user), 1));
            } else {        //User not member of group
                if (groupInfo.isPublic) {
                    actions.add(new ActionWeight(new JoinGroup(user, groupInfo.id), 50));
                    actions.add(new ActionWeight(new GetUserGroups(user), 15));
                    actions.add(new ActionWeight(new GetPopularGroups(user), 15));
                    actions.add(new ActionWeight(new GetInbox(user), 19));
                    actions.add(new ActionWeight(new Exit(user), 1));
                } else {
                    //TODO: Handle case of coming from venue info, need to pass venueId
                    return null;
                }
            }
            return getRandomAction(actions);
        } else {
            log.error("Failed to get group info for group: {}", groupId);
            return null;
        }
    }


    public static class GroupInfo {
        public Long id;
        public String name;
        public String description;
        @SerializedName("public")
        public Boolean isPublic;
        public Boolean isMember;
        public Long membersCount;
        public Boolean isUserSubscribed;

        public Long ageBucketMode;
        public Long musicTypeMode;
        public Long atmosphereTypeMode;
        public Long spendingLimitMode;

        @Override
        public String toString() {
            return "GroupInfo{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", description='" + description + '\'' +
                    ", isPublic=" + isPublic +
                    ", isMember=" + isMember +
                    ", membersCount=" + membersCount +
                    ", isUserSubscribed=" + isUserSubscribed +
                    ", ageBucketMode=" + ageBucketMode +
                    ", musicTypeMode=" + musicTypeMode +
                    ", atmosphereTypeMode=" + atmosphereTypeMode +
                    ", spendingLimitMode=" + spendingLimitMode +
                    '}';
        }
    }
}
