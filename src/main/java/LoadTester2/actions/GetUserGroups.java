package LoadTester2.actions;

import LoadTester2.Request.Type.Request;
import LoadTester2.Request.params.CityTrending;
import LoadTester2.Request.params.Group;
import LoadTester2.Response.RequestResponse;
import LoadTester2.User;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.annotations.SerializedName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class GetUserGroups extends IgnightAction {
    private static final Logger log = LoggerFactory.getLogger(GetUserGroups.class);

    public GetUserGroups(User user) {
        super(user);
    }

    @Override
    public Request doAction() {
        return rG.getUserGroups(user);
    }

    @Override
    public Callable<RequestResponse> getNextAction(RequestResponse response) {
        if (response.success) {
            List<ReturnedGroup> groups = parseUserGroups(response.body);
            ArrayList<ActionWeight> actions = new ArrayList<ActionWeight>();
            actions.add(new ActionWeight(new CreateGroup(user, Group.createRandomGroup()), 10));
            if (groups.size() > 0) {
                actions.add(new ActionWeight(new GetTrending(user, new CityTrending(true)), 10));
                ReturnedGroup group = groups.get(rand.nextInt(groups.size()));
                actions.add(new ActionWeight(new GetGroupTrending(user, group.groupId), 20));
            } else {
                actions.add(new ActionWeight(new GetTrending(user, new CityTrending(true)), 30));
            }
            actions.add(new ActionWeight(new GetPopularGroups(user, 15), 30));
            actions.add(new ActionWeight(new GetInbox(user), 30));
            return getRandomAction(actions);
        } else {
            log.error("Failed to get user groups: {}, response: {}", user, response.body);
            return null;
        }
    }

    private List<ReturnedGroup> parseUserGroups(JsonElement element) {
        List<ReturnedGroup> groups = new ArrayList<ReturnedGroup>();
        if (element.isJsonArray()) {
            JsonArray groupArray = element.getAsJsonArray();
            for (JsonElement group : groupArray) {
                groups.add(gson.fromJson(group.getAsJsonObject(), ReturnedGroup.class));
            }
        }
        return groups;
    }

    public static class ReturnedGroup {
        public String name;
        public Long groupId;
        public String description;
        @SerializedName("private")
        public boolean isPrivate;
        public boolean newGroupBuzzAvailable;
        public Long adminId;
        public Long members;
    }
}
