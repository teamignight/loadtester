package LoadTester2.actions;

import LoadTester2.Request.Type.Request;
import LoadTester2.Request.params.Invite;
import LoadTester2.Response.RequestResponse;
import LoadTester2.User;

import java.util.concurrent.Callable;

public class InviteUser extends IgnightAction {
    public Invite invite;

    public InviteUser(User user, Invite inite) {
        super(user);
        this.invite = inite;
    }

    @Override
    public Request doAction() {
        return rG.inviteUser(user, invite);
    }

    @Override
    public Callable<RequestResponse> getNextAction(RequestResponse response) {
        return null;
    }
}
