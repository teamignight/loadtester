package LoadTester2.actions;

import LoadTester2.Request.Type.Request;
import LoadTester2.Request.params.CityTrending;
import LoadTester2.Request.params.GetBuzzParams;
import LoadTester2.Request.params.Group;
import LoadTester2.Request.params.PostBuzzParams;
import LoadTester2.Response.RequestResponse;
import LoadTester2.User;
import com.fasterxml.jackson.annotation.JsonValue;
import com.google.gson.annotations.SerializedName;
import junit.framework.Assert;
import junit.framework.ComparisonFailure;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Callable;

import static junit.framework.TestCase.assertEquals;

public class CreateGroup extends IgnightAction {
    private static final Logger log = LoggerFactory.getLogger(CreateGroup.class);

    Group group;
    public CreateGroup(User user, Group group) {
        super(user);
        this.group = group;
    }

    @Override
    public Request doAction() {
        return rG.createGroup(user, group);
    }

    @Override
    public Callable<RequestResponse> getNextAction(RequestResponse response) {
        if (response.success) {
            CreateGroupResponse createGroupResponse = gson.fromJson(response.body, CreateGroupResponse.class);
            try {
                createGroupResponse.validate(user, group);
            } catch (ComparisonFailure e) {
                log.error("Group Was Not What was created: Group: {}, Response: {}, CreatedGroupResponse: {}", group, response.body, createGroupResponse);
                return null;
            }
            user.addGroup(createGroupResponse);
            return new GetGroupTrending(user, createGroupResponse.id);
        } else {
            log.error("Failed to create group: {}", response.requestMsg);
            return new Login(user);
        }
    }

    public static class CreateGroupResponse {
        public long id;
        @SerializedName("n")
        public String name;
        @SerializedName("c")
        public Long cityId;
        @SerializedName("d")
        public String description;
        @SerializedName("t")
        public boolean isPrivate;
        @SerializedName("admin")
        public long adminId;

        @Override
        public String toString() {
            return "CreateGroupResponse{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", cityId=" + cityId +
                    ", description='" + description + '\'' +
                    ", isPrivate=" + isPrivate +
                    ", adminId=" + adminId +
                    '}';
        }

        public void validate(User user, Group group) {
            assertEquals(group.groupName, name);
            assertEquals(group.groupDescription, description);
            assertEquals(group.cityId, cityId);
            assertEquals(group.isPrivate, isPrivate);
            assertEquals(user.userId, adminId);
        }
    }
}
