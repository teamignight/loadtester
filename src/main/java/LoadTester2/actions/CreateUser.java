package LoadTester2.actions;

import LoadTester2.Request.Type.Request;
import LoadTester2.Response.RequestResponse;
import LoadTester2.User;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Callable;

public class CreateUser extends IgnightAction {
    static final Logger log = LoggerFactory.getLogger(CreateUser.class);
    public CreateUser(User user) {
        super(user);
    }

    @Override
    public Request doAction() {
        return rG.createUser(user);
    }

    @Override
    public Callable<RequestResponse> getNextAction(RequestResponse response) {
        if (response.success) {
            CreateUserResponse createUserResponse = gson.fromJson(response.body, CreateUserResponse.class);
            user.userId = createUserResponse.id;
            log.info("Created User: {}", user);
            return new SetUser(user);
        }
        log.error("Failed to create user: {}, response: {}", user, response.body);
        return null;
    }

    public static class CreateUserResponse {
        public long id;
    }
}
