package LoadTester2.actions;

import LoadTester.Utils;
import LoadTester2.Request.RequestGenerator;
import LoadTester2.Request.RequestSender;
import LoadTester2.Request.Type.Request;
import LoadTester2.Response.RequestResponse;
import LoadTester2.User;
import com.amazonaws.services.elasticbeanstalk.model.RequestEnvironmentInfoRequest;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;

public abstract class IgnightAction implements Callable<RequestResponse> {
    private static final Logger log = LoggerFactory.getLogger(IgnightAction.class);
    public static Random rand = new Random();
    public static Gson gson = new Gson();
    public static RequestGenerator rG = new RequestGenerator();
    public RequestResponse response;
    public User user;

    public IgnightAction(User user) {
        this.user = user;
    }

    @Override
    public RequestResponse call() throws Exception {
        Request request = null;
        try {
            request = doAction();
            response = RequestSender.send(request);
            if (response != null) {
                response.nextAction = getNextAction(response);
                return response;
            }
            return null;
        } catch (Exception e) {
            log.error("Failed to run : request: {}, response: {}", request, response, e);
            return null;
        }
    }

    public abstract Request doAction();

    public abstract Callable<RequestResponse> getNextAction(RequestResponse response);

    public static class ActionWeight {
        public Callable<RequestResponse> action;
        public int weight;

        public ActionWeight(Callable<RequestResponse> action, int weight) {
            this.action = action;
            this.weight = weight;
        }
    }

    public Callable<RequestResponse> getRandomAction(List<ActionWeight> actionWeights) {
        ArrayList<Callable<RequestResponse>> actions = new ArrayList<Callable<RequestResponse>>();
        for (int i = 0; i < actionWeights.size(); i++) {
            ActionWeight actionWeight = actionWeights.get(i);
            for (int j = 0; j < actionWeight.weight; j++) {
                actions.add(actionWeight.action);
            }
        }
        return actions.get(rand.nextInt(actions.size()));
    }
}
