package LoadTester2.actions;

import LoadTester2.Request.Type.Request;
import LoadTester2.Response.RequestResponse;
import LoadTester2.User;

import java.util.Random;
import java.util.concurrent.Callable;

public class SearchForGroups extends IgnightAction {
    private static String alphabet = "abcdefghijklmnopqrstuvwxyz";
    private static Random rand = new Random();

    public String search;

    public SearchForGroups(User user) {
        super(user);
        int length = rand.nextInt(5) + 1;
        for(int i = 0; i < length; i++){
            this.search += alphabet.charAt(rand.nextInt(alphabet.length()));
        }
    }

    public SearchForGroups(User user, String search) {
        super(user);
    }

    @Override
    public Request doAction() {
        return rG.searchForGroups(user, search);
    }

    @Override
    public Callable<RequestResponse> getNextAction(RequestResponse response) {
        return null;
    }
}
