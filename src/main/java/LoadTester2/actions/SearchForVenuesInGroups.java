package LoadTester2.actions;

import LoadTester2.Request.Type.Request;
import LoadTester2.Request.params.SearchVenueInGroup;
import LoadTester2.Response.RequestResponse;
import LoadTester2.User;
import com.google.gson.JsonElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.concurrent.Callable;

public class SearchForVenuesInGroups extends IgnightAction {
    static final Logger log = LoggerFactory.getLogger(SearchForVenuesInGroups.class);

    public SearchVenueInGroup searchVenueInGroup;
    public SearchForVenuesInGroups(User user, SearchVenueInGroup searchVenueInGroup) {
        super(user);
        this.searchVenueInGroup = searchVenueInGroup;
    }

    @Override
    public Request doAction() {
        return rG.searchVenuesInGroups(user, searchVenueInGroup);
    }

    @Override
    public Callable<RequestResponse> getNextAction(RequestResponse response) {
        if (response.success) {
            ArrayList<GetGroupTrending.GroupTrending> venues = parseResponse(response.body);
        }
        log.error("Failed to search for venues in group: {}, user: {}, reason: {}", searchVenueInGroup.groupId, user, response.reason);
        return null;
    }

    private ArrayList<GetGroupTrending.GroupTrending> parseResponse(JsonElement element) {
        ArrayList<GetGroupTrending.GroupTrending> venues = new ArrayList<GetGroupTrending.GroupTrending>();
        if (element.isJsonArray()) {
            for (JsonElement elem : element.getAsJsonArray()) {
                venues.add(gson.fromJson(elem, GetGroupTrending.GroupTrending.class));
            }
        }
        return venues;
    }
}
