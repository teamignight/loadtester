package LoadTester2.actions;

import LoadTester2.Request.Type.Request;
import LoadTester2.Request.params.CityTrending;
import LoadTester2.Response.RequestResponse;
import LoadTester2.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.concurrent.Callable;

public class SetVenueActivity extends IgnightAction {
    private final static Logger log = LoggerFactory.getLogger(SetVenueActivity.class);

    public long venueId;
    public long activity;

    public SetVenueActivity(User user, long activity) {
        super(user);
        this.venueId = rand.nextInt(1000 + 1);
        this.activity = activity;
    }

    @Override
    public Request doAction() {
        return rG.setVenueActivity(user, venueId, activity);
    }

    @Override
    public Callable<RequestResponse> getNextAction(RequestResponse response) {
        if (response.success) {
            ArrayList<ActionWeight> actions = new ArrayList<ActionWeight>();
            actions.add(new ActionWeight(new GetTrending(user, new CityTrending(true)), 90));
            actions.add(new ActionWeight(new Exit(user), 10));
            return getRandomAction(actions);
        } else {
            log.error("Failed to get set activity: {}, for venue: {}, reason: {}", activity, venueId, response.reason);
            return null;
        }
    }
}
