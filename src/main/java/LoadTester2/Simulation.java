package LoadTester2;

import LoadTester.DataCollector;
import LoadTester.Utils;
import LoadTester2.Response.RequestResponse;
import LoadTester2.actions.CreateUser;
import com.google.common.util.concurrent.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Simulation {
    public static final String connectionUrl = "http://production.chicago.ignight.com/ignight_server";

    static final Logger log = LoggerFactory.getLogger(Simulation.class);
    public final static ListeningExecutorService executors = MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors()));
    public static final Random rand = new Random();

    public static void main(String... args) throws Exception {
//        clear();
//        Utils.sleep(20000);
        simulate();
    }

    public static void clear() {
        Utils.resetInstance();
        log.info("Finished Clearing");
    }

    public static void simulate() {
        try {
            final long total = 10000;
            for (int i  = 0; i < total; i++) {
                ListenableFuture<RequestResponse> future = executors.submit(new CreateUser(User.generateRandom()));
                Futures.addCallback(future, new IgnightActionCallback(executors));
            }
        } catch (Exception e) {
            log.error("Failed to run simulation", e);
        }
    }
}
