package LoadTester2.Response;

import LoadTester2.Request.Type.Request;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Callable;

public class RequestResponse {
    private final static Logger log = LoggerFactory.getLogger(RequestResponse.class);
    public Request request;
    public JsonObject requestMsg;

    public boolean success = false;
    public String reason;
    public JsonElement body;

    public Callable<RequestResponse> nextAction;

    public RequestResponse(Request request, JsonObject msg) {
        this.request = request;
        this.requestMsg = msg;
        processMsg();
    }

    private void processMsg() {
        if (requestMsg.has("res")) {
            success = requestMsg.get("res").getAsBoolean();
            if (success) {
                if (requestMsg.has("body")) {
                    body = requestMsg.get("body");
                }
            } else {
                reason = requestMsg.get("reason").getAsString();
            }
        } else {
            throw new RequestResponseException("'res' field missing from request : " + request.url + " , body: " + request.msg + " response.");
        }
    }

    @Override
    public String toString() {
        return "RequestResponse{" +
                "request=" + request +
                ", requestMsg=" + requestMsg +
                ", success=" + success +
                ", reason='" + reason + '\'' +
                ", body=" + body +
                ", nextAction=" + nextAction +
                '}';
    }
}
