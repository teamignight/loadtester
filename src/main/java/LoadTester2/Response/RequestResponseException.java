package LoadTester2.Response;

public class RequestResponseException extends IllegalArgumentException {
    public RequestResponseException(String s) {
        super(s);
    }
}
