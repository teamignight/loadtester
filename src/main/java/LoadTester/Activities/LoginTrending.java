package LoadTester.Activities;

import LoadTester.DataCollector;
import LoadTester.UserFunctions;
import LoadTester2.User;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoginTrending extends IgnightActivity {
    private static final Logger log = LoggerFactory.getLogger(LoginTrending.class);

    @Override
    public void run() {
        try {
            User user = DataCollector.getRandomUser();
            if (user != null) {
                Object lock = DataCollector.getUserLock(user.userId);
                synchronized (lock) {
                    JSONObject jsonObject = UserFunctions.login(user);
                    if (!jsonObject.getBoolean("res")) {
                        log.error("Login failed for user: {}", user);
                    }


                }
            }
        } catch (Exception e) {

        }
    }
}
