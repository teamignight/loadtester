package LoadTester.Validators;


import LoadTester.Activities.IgnightActivity;
import LoadTester.DataCollector;
import LoadTester.MsgObjects.User;
import LoadTester.UserFunctions;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * - Create User
 * - Set User Information
 * - Register User
 */
public class CompleteFullRegistration extends IgnightActivity {
    private static final Logger log = LoggerFactory.getLogger(CompleteFullRegistration.class);

    @Override
    public void run() {
        try {
            User user = User.generateRandom();
            JSONObject jsonResult = UserFunctions.createUser(user);
            if (jsonResult.getBoolean("res")) {
                DataCollector.updateSuccess(User.getCreateUserUrl());

                user.userId = jsonResult.getJSONObject("body").getLong("id");
                jsonResult = UserFunctions.setUser(user);

                if (jsonResult.getBoolean("res")) {
                    DataCollector.updateSuccess(User.getSetUserUrl(user.userId));

                    jsonResult = UserFunctions.getUser(user);
                    if (jsonResult.getBoolean("res")) {
                        DataCollector.updateSuccess(User.getGetUserUrl(user.userId));
                        if (user.isUserInfoCorrect(jsonResult)) {
//                            DataCollector.addNewUser(user);
                        } else {
                            log.error("Returned User Not The Same: User: {}, ReturnedUser: {}", user, jsonResult);
                        }
                    } else {
                        log.error("Data not saved yet for user: {}, {}", user, jsonResult);
                        DataCollector.updateFailures(User.getGetUserUrl(user.userId));
                    }
                } else {
                    log.error("Failed to Set User Information: {}", user);
                    DataCollector.updateFailures(User.getSetUserUrl(user.userId));
                }
            } else {
                log.error("Call to uri = {}, returned: {}", "/user", jsonResult);
                DataCollector.updateFailures(User.getCreateUserUrl());
            }
        } catch (Exception e) {
            log.error("Failed to create user", e);
        }
    }
}
