package LoadTester;

import LoadTester2.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class DataCollector {
    private static final Logger log = LoggerFactory.getLogger(DataCollector.class);
    private static ConcurrentHashMap<String, AtomicLong> success = new ConcurrentHashMap<String, AtomicLong>();
    private static ConcurrentHashMap<String, AtomicLong> failures = new ConcurrentHashMap<String, AtomicLong>();

    public static void updateSuccess(String url) {
        updateCounter(success, url);
    }

    public static void updateFailures(String url) {
        updateCounter(failures, url);
    }

    public static long getSuccess(String url) {
        return getCounter(success, url);
    }

    public static long getFailures(String url) {
        return getCounter(failures, url);
    }

    private static long getCounter(ConcurrentHashMap<String, AtomicLong> counter, String url) {
        AtomicLong c = counter.get(url);
        if (c == null) {
            return 0;
        } else {
            return c.get();
        }
    }

    private static void updateCounter(ConcurrentHashMap<String, AtomicLong> counter, String url) {
        AtomicLong atomicLong = counter.get(url);
        if (atomicLong == null) {
            atomicLong = new AtomicLong(0);
            AtomicLong other = counter.putIfAbsent(url, atomicLong);
            if (other != null) {
                atomicLong = other;
            }
        }
        atomicLong.incrementAndGet();
    }


    private static ArrayList<Long> createdUsers = new ArrayList<Long>();
    private static Map<Long, Object> userLock = new HashMap<Long, Object>();
    private static Map<Long, LoadTester2.User> users = new HashMap<Long, LoadTester2.User>();

    public static long totalUsers() {
//        log.info("UserSize: {}", createdUsers.size());
        return createdUsers.size();
    }

    public static void addNewUser(LoadTester2.User user) {
//        log.info("Adding User: {}", user);/
        synchronized (userLock) {
            createdUsers.add(user.userId);
            userLock.put(user.userId, new Object());
            users.put(user.userId, user);
        }
    }

    public static User getRandomUser() {
        synchronized (userLock) {
            Random rand = new Random();
            long userId = createdUsers.get(rand.nextInt(createdUsers.size()));
            return users.get(userId);
        }
    }

    public static Object getUserLock(Long user) {
        synchronized (userLock) {
            return userLock.get(user);
        }
    }
}
