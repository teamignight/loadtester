package LoadTester;


import LoadTester2.Simulation;
import com.google.gson.Gson;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class HTTPRequestSender {
    private static final Logger log = LoggerFactory.getLogger(HTTPRequestSender.class);
    private static String destinationUrl = Simulation.connectionUrl;

    public static String sendPost(String urlString, String postData)  {
        try {
            URL url = new URL(destinationUrl + urlString);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            connection.setRequestProperty("Content-Length", String.valueOf(postData.length()));
            connection.setRequestProperty("X-IGNIGHT-TOKEN", "428CD392-C394-4237-A8B8-E95564333C44");

            // Write data
            OutputStream os = connection.getOutputStream();
            os.write(postData.getBytes());

            // Read response
            StringBuilder responseSB = new StringBuilder();
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String line;
            while ((line = br.readLine()) != null)
                responseSB.append(line);

            // Close streams
            br.close();
            os.close();

            return responseSB.toString();
        } catch (IOException e) {
            log.error("Failed to execute POST uri = {}, data = {}", urlString, postData, e);
            return null;
        }
    }

    public static String sendPost(String urlString, JSONObject postData) throws IOException {

        URL url = new URL(destinationUrl + urlString);

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoOutput(true);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
        connection.setRequestProperty("Content-Length", String.valueOf(postData.length()));
        connection.setRequestProperty("X-IGNIGHT-TOKEN", "428CD392-C394-4237-A8B8-E95564333C44");

        // Write data
        OutputStream os = connection.getOutputStream();
        os.write(postData.toString().getBytes());

        // Read response
        StringBuilder responseSB = new StringBuilder();
        BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

        String line;
        while ((line = br.readLine()) != null) {
            responseSB.append(line);
        }

        // Close streams
        br.close();
        os.close();

        return responseSB.toString();
    }

    public static String sendPut(String urlString, String putData)  {
        try {
            URL url = new URL(destinationUrl + urlString);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("PUT");
            connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            connection.setRequestProperty("Content-Length", String.valueOf(putData.length()));
            connection.setRequestProperty("X-IGNIGHT-TOKEN", "428CD392-C394-4237-A8B8-E95564333C44");

            // Write data
            OutputStream os = connection.getOutputStream();
            os.write(putData.getBytes());

            // Read response
            StringBuilder responseSB = new StringBuilder();
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String line;
            while ((line = br.readLine()) != null) {
                responseSB.append(line);
            }

            // Close streams
            br.close();
            os.close();

            return responseSB.toString();
        } catch (IOException e) {
            log.error("Failed to execute PUT uri = {}, data = {}", urlString, putData, e);
            return null;
        }
    }

    public static String sendGet(String urlString, Map<Object, Object> getData) {
        try {
            String fullUrl = destinationUrl + urlString + "?";
            for (Map.Entry<Object, Object> entry : getData.entrySet()) {
                Object key = entry.getKey();
                Object value = entry.getValue();
                fullUrl = fullUrl + key + "=" + value + "&";
            }
            fullUrl = StringUtils.left(fullUrl, fullUrl.length() - 1);
            URL url = new URL(fullUrl);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            connection.setRequestProperty("X-IGNIGHT-TOKEN", "428CD392-C394-4237-A8B8-E95564333C44");

            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            return response.toString();
        } catch (IOException e) {
            log.error("Failed to execute GET uri = {}, data = {}", urlString, getData, e);
            return null;
        }
    }
}