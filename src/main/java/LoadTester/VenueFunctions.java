package LoadTester;

import com.google.gson.Gson;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class VenueFunctions implements Runnable {
    public static String urlString = "http://dev.chicago.ignight.com/ignight_server";
    public static Long cityId = 0L;
    public static String alphabet = "abcdefghijklmnopqrstuvwxyz";
    static Random rand = new Random();


    public VenueFunctions() {
        // TODO Auto-generated constructor stub
    }

    /********************************POST REQUESTS********************************/

    public static boolean setUserActivity() throws IOException, JSONException {
        Map<String, String> postMap = new HashMap<String, String>();
        Map<String, Map<String, String>> msgMap = new HashMap<String, Map<String, String>>();

        Long userId = UserFunctions.createdUsers.get(UserFunctions.createdUsers.size() > 1? rand.nextInt(UserFunctions.createdUsers.size()-1):0);
        Long venueId = (long) rand.nextInt(3027);
        String url = "/venue/activity/" + venueId;
        postMap.put("userId", userId.toString());
        postMap.put("userActivity",  "" + (rand.nextInt(2) - 1));

        msgMap.put("msg", postMap);
        Gson gson = new Gson();
        String postData = gson.toJson(msgMap);
        String output = HTTPRequestSender.sendPost(url, postData);
        JSONObject jsonResult = new JSONObject(output);
        return jsonResult.getBoolean("res");
    }

    public static boolean reportVenueDetailsError() throws IOException, JSONException{
        Map<String, String> postMap = new HashMap<String, String>();
        Map<String, Map<String, String>> msgMap = new HashMap<String, Map<String, String>>();

        Long userId = UserFunctions.createdUsers.get(UserFunctions.createdUsers.size() > 1? rand.nextInt(UserFunctions.createdUsers.size()-1):0);
        Long venueId = (long) rand.nextInt(3027);

        String url = "/venue/report/error/" + venueId;

        postMap.put("userId", userId.toString());
        postMap.put("venueName",  "" + "testVenueName");
        postMap.put("cityId",  "" + cityId.toString());
        postMap.put("address",  "123 State Street");
        postMap.put("number",  "212-545-4747");
        postMap.put("url",  "www.test.com");
        postMap.put("comment",  "from test script. You better change it.");

        msgMap.put("msg", postMap);
        Gson gson = new Gson();
        String postData = gson.toJson(msgMap);
        String output = HTTPRequestSender.sendPost(url, postData);
        JSONObject jsonResult = new JSONObject(output);
        return jsonResult.getBoolean("res");
    }

    public static boolean requestVenueAddition() throws IOException, JSONException{
        Map<String, String> postMap = new HashMap<String, String>();
        Map<String, Map<String, String>> msgMap = new HashMap<String, Map<String, String>>();

        Long userId = UserFunctions.createdUsers.get(UserFunctions.createdUsers.size() > 1? rand.nextInt(UserFunctions.createdUsers.size()-1):0);

        String url = "venue/report/add/" + userId;

        postMap.put("venueName",  "" + "testVenueName");
        postMap.put("cityId",  "" + cityId.toString());

        if(rand.nextBoolean()){
            postMap.put("address",  "123 State Street");
        }

        if(rand.nextBoolean()){
            postMap.put("number",  "212-545-4747");
        }

        if(rand.nextBoolean()){
            postMap.put("url",  "www.test.com");
        }

        if(rand.nextBoolean()){
            postMap.put("comment",  "from test script. You better change it.");
        }

        msgMap.put("msg", postMap);
        Gson gson = new Gson();
        String postData = gson.toJson(msgMap);
        String output = HTTPRequestSender.sendPost(url, postData);
        JSONObject jsonResult = new JSONObject(output);
        return jsonResult.getBoolean("res");
    }

    /*TODO: Add image upload later*/
    public static boolean addBuzz() throws IOException, JSONException{
        Map<String, String> postMap = new HashMap<String, String>();
        Map<String, Map<String, String>> msgMap = new HashMap<String, Map<String, String>>();

        Long userId = UserFunctions.createdUsers.get(UserFunctions.createdUsers.size() > 1? rand.nextInt(UserFunctions.createdUsers.size()-1):0);
        Long venueId = (long) rand.nextInt(3027);

        String url = "/buzz/add/venue/text/" + venueId;

        postMap.put("userId", userId.toString());
        postMap.put("buzzText",  "random buzz text");

        msgMap.put("msg", postMap);
        Gson gson = new Gson();
        String postData = gson.toJson(msgMap);
        String output = HTTPRequestSender.sendPost(url, postData);
        JSONObject jsonResult = new JSONObject(output);
        return jsonResult.getBoolean("res");
    }


    /********************************GET REQUESTS********************************/

    public static boolean getVenueDataForUser() throws IOException, JSONException{
        Map<Object, Object> getData = new HashMap<Object, Object>();

        Long userId = UserFunctions.createdUsers.get(UserFunctions.createdUsers.size() > 1? rand.nextInt(UserFunctions.createdUsers.size()-1):0);
        Long venueId = (long) rand.nextInt(3027);

        String url = "/venue/info/" + venueId;

        getData.put("userId", userId.toString());
        String output = HTTPRequestSender.sendGet(url, getData);
        JSONObject jsonResult = new JSONObject(output);
        return jsonResult.getBoolean("res");
    }

	/*
	public static boolean getLatestBuzz() throws IOException, JSONException{
		HashMap<String, String> getData = new HashMap<String, String>();

		getData.put("type", "getLatestBuzz");
		getData.put("venueId", "" + rand.nextInt(3027));
		getData.put("userId", UserFunctions.createdUsers.get(UserFunctions.createdUsers.size() > 1? rand.nextInt(UserFunctions.createdUsers.size()-1):0).toString());
		getData.put("lastBuzzId", "-1");
		getData.put("count", "" + (rand.nextInt(25) + 1));

		String output = HTTPRequestSender.sendGet(urlString, getData);
		JSONObject jsonResult = new JSONObject(output);
		return jsonResult.getBoolean("res");
	}
	*/

    public static boolean getBuzz() throws IOException, JSONException{
        Map<Object, Object> getData = new HashMap<Object, Object>();

        Long userId = UserFunctions.createdUsers.get(UserFunctions.createdUsers.size() > 1? rand.nextInt(UserFunctions.createdUsers.size()-1):0);
        Long venueId = (long) rand.nextInt(3027);

        String url = "/buzz/venue/" + venueId;

        getData.put("userId", userId.toString());
        getData.put("lastBuzzId", "-1");
        getData.put("count", "" + (rand.nextInt(25) + 1));

        String output = HTTPRequestSender.sendGet(url, getData);
        JSONObject jsonResult = new JSONObject(output);
        return jsonResult.getBoolean("res");
    }

    public static boolean getVenueBuzzImages() throws IOException, JSONException{
        Map<Object, Object> getData = new HashMap<Object, Object>();

        Long userId = UserFunctions.createdUsers.get(UserFunctions.createdUsers.size() > 1? rand.nextInt(UserFunctions.createdUsers.size()-1):0);
        Long venueId = (long) rand.nextInt(3027);

        String url = "/images/venue/" + venueId;

        getData.put("userId", userId.toString());
        getData.put("buzzImageStartId", "0");
        getData.put("noOfBuzzImages", "" + rand.nextInt(50));

        String output = HTTPRequestSender.sendGet(url, getData);
        JSONObject jsonResult = new JSONObject(output);
        return jsonResult.getBoolean("res");
    }

    @Override
    public void run() {
        int noOfOperations = 100;
        for(int i = 0; i < noOfOperations; i++){
            int randomOperation = rand.nextInt(7);

            try{
                switch (randomOperation){
                    case 1: System.out.println("setUserActivity:" + VenueFunctions.setUserActivity());
                        break;

                    case 2: System.out.println("reportVenueDetailsError:" + VenueFunctions.reportVenueDetailsError());
                        break;

                    case 3: System.out.println("addBuzz:" + VenueFunctions.addBuzz());
                        break;

                    case 4: System.out.println("getVenueDataForUser:" + VenueFunctions.getVenueDataForUser());
                        break;

                    case 5: System.out.println("getLatestBuzz:" + VenueFunctions.getBuzz());
                        break;

                    case 6: System.out.println("getBuzz:" + VenueFunctions.getBuzz());
                        break;

                    case 7: System.out.println("getVenueBuzzImages:" + VenueFunctions.getVenueBuzzImages());
                        break;

                    case 0: System.out.println("requestVenueAddition:" + VenueFunctions.requestVenueAddition());
                        break;

                }
            }catch(Exception e){

            }
        }

    }

}
