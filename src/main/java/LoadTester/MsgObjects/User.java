package LoadTester.MsgObjects;

import LoadTester.Utils;
import LoadTester2.Request.Type.PostMsg;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

public class User {
    private static final Logger log = LoggerFactory.getLogger(User.class);

    public String username;
    public String password;
    public String email;
    public long userId;

    public int cityId;
    public int ageId;
    public int genderId;
    public ArrayList<Long> music;
    public ArrayList<Long> atmosphere;
    public int spendingLimit;
    public boolean notifyOfGroupInvites = true;
    public String profilePictureUrl = null;

    private static Random rand = new Random();
    private static String alphabet = "abcdefghijklmnopqrstuvwxyz0123456789";

    public static User generateRandom() {
        User user = new User();
        for (int i = 0; i < 10; i++) {
            user.username += alphabet.charAt(rand.nextInt(35));
        }
        user.email = UUID.randomUUID().toString().substring(15);
        user.password = "password";

        user.cityId = 0;
        user.genderId = rand.nextInt(1);
        user.ageId = rand.nextInt(3);
        user.music = Utils.getRandomMusicArrayList();
        user.atmosphere = Utils.getRandomAtmosphereArrayList();
        user.spendingLimit = rand.nextInt(3);
        return user;
    }

    public boolean isUserInfoCorrect(JSONObject returnedUser) {
        try {
            returnedUser = returnedUser.getJSONObject("body");
            if (userId != returnedUser.getLong("id")) {
                log.info("User Id's Dont Match [{}] - User: {}, Returned: {}", userId, userId, returnedUser.getLong("id"));
                return false;
            }
            if (returnedUser.getBoolean("userInfoSet")) {
                if (returnedUser.getInt("cityId") != cityId) {
                    log.info("City Id's Dont Match [{}] - User: {}, Returned: {}", userId, cityId, returnedUser.getInt("cityId"));
                    return false;
                }

                if (!returnedUser.getString("userName").equals(username)) {
                    log.info("UserName Dont Match [{}] - User: {}, Returned: {}", userId, username, returnedUser.getString("userName"));
                    return false;
                }

                if (returnedUser.getInt("genderId") != genderId) {
                    log.info("GenderId Dont Match [{}] - User: {}, Returned: {}", userId, genderId, returnedUser.getInt("genderId"));
                    return false;
                }

                if (!returnedUser.getString("email").equals(email)) {
                    log.info("Email Dont Match [{}] - User: {}, Returned: {}", userId, email, returnedUser.getString("email"));
                    return false;
                }

                if (returnedUser.getBoolean("notifyOfGroupInvites") != notifyOfGroupInvites) {
                    log.info("NotifyOfGroupInvites Dont Match [{}] - User: {}, Returned: {}", userId, notifyOfGroupInvites, returnedUser.getBoolean("notifyOfGroupInvites"));
                    return false;
                }

                if (returnedUser.getInt("age") != ageId) {
                    log.info("Age Dont Match [{}] - User: {}, Returned: {}", userId, ageId, returnedUser.getInt("age"));
                    return false;
                }

                if (returnedUser.getInt("spending") != spendingLimit) {
                    log.info("Spending Limit Dont Match [{}] - User: {}, Returned: {}", userId, spendingLimit, returnedUser.getInt("spending"));
                    return false;
                }

                if (!compareArrays(music, returnedUser.getJSONArray("music"))) {
                    log.info("Music Doesnt Match [{}] - User: {}, Returned: {}", userId, music, returnedUser.getJSONArray("music"));
                    return false;
                }

                if (!compareArrays(atmosphere, returnedUser.getJSONArray("atmosphere"))) {
                    log.info("Atmosphere Doesnt Match [{}] - User: {}, Returned: {}", userId, atmosphere, returnedUser.getJSONArray("atmosphere"));
                    return false;
                }

                return true;
            } else {
                return true;
            }
        } catch (JSONException e) {
            log.error("Failed to parse returnedUser: {}", returnedUser, e);
        }
        return false;
    }

    public boolean checkForUserInfoSet(JSONObject returnedUser, boolean check) {
        try {
            returnedUser = returnedUser.getJSONObject("body");
            if (userId != returnedUser.getLong("id")) {
                log.info("User Id's Dont Match [{}] - User: {}, Returned: {}", userId, userId, returnedUser.getLong("id"));
                return false;
            }
            if ((returnedUser.getBoolean("userInfoSet") != check)) {
                log.info("UserInfoSet [{}], Expected: {}, Actual: {}", userId, check, returnedUser.getBoolean("userInfoSet"));
                return false;
            }
            return true;
        } catch (JSONException e) {
            log.error("Failed to parse returnedUser: {}", returnedUser, e);
        }
        return false;
    }

    private boolean compareArrays(ArrayList<Long> selection, JSONArray returned) throws JSONException {
        for (int i = 0; i < selection.size(); i++) {
            long s = selection.get(i);
            long r = returned.getLong(i);
            if (s != r) {
                return false;
            }
        }
        return true;
    }

    public String generateCreateUserMsg() {
        PostMsg msg = new PostMsg().set("userName", username)
                .set("email", email)
                .set("password", password);
        return msg.getPostData();
    }

    public String generateUpdateUserMsg() {
        PostMsg msg = new PostMsg().set("age", ageId)
                .set("music", music)
                .set("atmosphere", atmosphere)
                .set("spendingLimit", spendingLimit)
                .set("email", email)
                .set("notifyOfGroupInvites", notifyOfGroupInvites);
        return msg.getPostData();
    }

    public String generateSetUserMsg() {
        PostMsg msg = new PostMsg().set("userId", userId)
                .set("cityId", cityId)
                .set("genderId", genderId)
                .set("age", ageId)
                .set("music", music)
                .set("atmosphere", atmosphere)
                .set("spendingLimit", spendingLimit);
        return msg.getPostData();
    }

    public String generateLoginMsg() {
        return new PostMsg().set("userName", username).set("password", password).getPostData();
    }

    public String generateLogoutMsg() {
        return new PostMsg().getPostData();
    }

    public static String getCreateUserUrl() {
        return "/user";
    }

    public static String getSetUserUrl(Long userId) {
        return "/user/" + userId;
    }

    public static String getGetUserUrl(Long userId) {
        return "/user/" + userId;
    }

    public static String getUpdateUserUrl(Long userId) {
        return "/user/" + userId;
    }

    public static String getLoginUrl() {
        return "/login";
    }

    public static String getLogoutUrl(Long userId) {
        return "/logout/" + userId;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", userId=" + userId +
                ", cityId=" + cityId +
                ", ageId=" + ageId +
                ", genderId=" + genderId +
                ", music=" + music +
                ", atmosphere=" + atmosphere +
                ", spendingLimit=" + spendingLimit +
                ", notifyOfGroupInvites=" + notifyOfGroupInvites +
                ", profilePictureUrl='" + profilePictureUrl + '\'' +
                '}';
    }
}
