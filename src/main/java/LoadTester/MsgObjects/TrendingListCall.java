package LoadTester.MsgObjects;

import LoadTester2.Request.Type.GetMsg;

import java.util.Map;
import java.util.Random;

public class TrendingListCall {
    public Long music_filter = null;
    public Double searchRadius = null;
    public Double latitude = null;
    public Double longitude = null;
    public String search = null;

    private double latitudeCenter = 41.8020;
    private double longitudeCenter = -87.6278;
    private double metersPerMile = 1609.344;

    public static Map<Object, Object> generateRegularTrendingCall() {
        return new GetMsg().set("count", 15).getParams();
    }

    public static Map<Object, Object> generateUpvoteCall() {
        return new GetMsg().set("count", 15).set("upvote_filter", true).getParams();
    }

    public static Map<Object, Object> generateRandomMusicFilterCall() {
        Random rand = new Random();
        return generateMusicFilterCall(rand.nextInt(15));
    }

    public static Map<Object, Object> generateMusicFilterCall(long music_filter) {
        return new GetMsg().set("count", 15).set("music_filter", music_filter).getParams();
    }

    public static Map<Object, Object> generateSearchCall(String s) {
        return new GetMsg().set("count", 15).set("search", s).getParams();
    }

    public static String getCityTrendingUrl(Long userId) {
        return "/trending/" + userId;
    }
}