package LoadTester;

import LoadTester2.Request.Type.PostMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Utils {
    private static final Logger log = LoggerFactory.getLogger(Utils.class);
    private static final ArrayList<Long> musicArrayList = new ArrayList<Long>();
    private static final ArrayList<Long> atmosphereArrayList = new ArrayList<Long>();
    public static Random rand = new Random();
    public static String alphabet = "abcdefghijklmnopqrstuvwxyz";

    static {
        for(long i=0; i<15; i++){
            musicArrayList.add(i);
        }
        for(long i=0; i<8; i++){
            atmosphereArrayList.add(i);
        }
    }

    public static void clearDailyData(boolean force) {
        log.info("Clearing Daily Data - force={}", force);
        PostMsg msg = new PostMsg().set("cityId", 0)
                                   .set("force", force)
                                   .set("dailyDataSecretKey", "spduankzu7edb1eumkpd");
        String url = "/data/clear";
        HTTPRequestSender.sendPost(url, msg.getPostData());
    }

    public static void resetInstance() {
        log.info("Resetting Instance..");
        PostMsg msg = new PostMsg().set("cityId", 0)
                                   .set("dailyDataSecretKey", "spduankzu7edb1eumkpd")
                                   .set("type", "reset");
        String url = "/data";
        HTTPRequestSender.sendPost(url, msg.getPostData());
    }

    public static ArrayList<Long> getRandomMusicArrayList(){
        Collections.shuffle(musicArrayList);
        ArrayList<Long> music = new ArrayList<Long>();
        Random rand = new Random();
        boolean done = false;
        for (int i = 0; i < 3; i++) {
            if (i == 0) {
                music.add(musicArrayList.get(i));
            } else {
                if (!done && rand.nextBoolean()) {
                    music.add(musicArrayList.get(i));
                } else {
                    done = true;
                    music.add(-1l);
                }
            }
        }
        return music;
    }

    public static ArrayList<Long> getRandomAtmosphereArrayList() {
        Collections.shuffle(atmosphereArrayList);
        ArrayList<Long> atmosphere = new ArrayList<Long>();
        Random rand = new Random();
        boolean done = false;
        for (int i = 0; i < 3; i++) {
            if (i == 0) {
                atmosphere.add(atmosphereArrayList.get(i));
            } else {
                if (!done && rand.nextBoolean()) {
                    atmosphere.add(atmosphereArrayList.get(i));
                } else {
                    done = true;
                    atmosphere.add(-1l);
                }
            }
        }
        return atmosphere;
    }

    public static void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static String getRandomName(long size) {
        String name = "";
        for(int i = 0; i < size + 1; i++){
            name += alphabet.charAt(rand.nextInt(alphabet.length()));
        }
        return name;
    }
}