package LoadTester;

import LoadTester.MsgObjects.TrendingListCall;
import LoadTester.MsgObjects.User;
import LoadTester2.Request.RequestGenerator;
import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class UserFunctions implements Runnable {
    private static final Logger log = LoggerFactory.getLogger(UserFunctions.class);
    public static Long cityId = 0L;
    public static String alphabet = "abcdefghijklmnopqrstuvwxyz0123456789";
    public static int maxNumberOfUsers = 100000;
    static Random rand = new Random();
    public static ArrayList<Long> createdUsers = new ArrayList<Long>();

    public static JSONObject createUser(User user) {
        String output = HTTPRequestSender.sendPost(User.getCreateUserUrl(), user.generateCreateUserMsg());
        try {
            return new JSONObject(output);
        } catch (JSONException e) {
            log.error("[Create User] : Failed to parse output: {}", output, e);
            return null;
        }
    }

    public static JSONObject setUser(User user) {
        String output = HTTPRequestSender.sendPut(User.getSetUserUrl(user.userId), user.generateSetUserMsg());
        try {
            return new JSONObject(output);
        } catch (JSONException e) {
            log.error("[Set User] : Failed to parse output: {}", output, e);
            return null;
        }
    }

    public static JSONObject getUser(User user) {
        String output = HTTPRequestSender.sendGet(User.getGetUserUrl(user.userId), new HashMap<Object, Object>());
        try {
            return new JSONObject(output);
        } catch (JSONException e) {
            log.error("[Get User] : Failed to parse output: {}", output, e);
            return null;
        }
    }

    public static JSONObject updateUser(User user) {
        String output = HTTPRequestSender.sendPost(User.getUpdateUserUrl(user.userId), user.generateUpdateUserMsg());
        try {
            return new JSONObject(output);
        } catch (JSONException e) {
            log.error("[Update User] : Failed to parse output: {}", output, e);
            return null;
        }
    }

    public static JSONObject login(LoadTester2.User user) {
        String output = HTTPRequestSender.sendPost(User.getLoginUrl(), new RequestGenerator().login(user).msg.msg.toString());
        try {
            return new JSONObject(output);
        } catch (JSONException e) {
            log.error("[Login User] : Failed to parse output: {}", output, e);
            return null;
        }
    }

    public static JSONObject logout(User user) {
        String output = HTTPRequestSender.sendPost(User.getLogoutUrl(user.userId), user.generateLogoutMsg());
        try {
            return new JSONObject(output);
        } catch (JSONException e) {
            log.error("[Login User] : Failed to parse output: {}", output, e);
            return null;
        }
    }

    public static JSONObject getRegularTrending(User user) {
        String output = HTTPRequestSender.sendGet(TrendingListCall.getCityTrendingUrl(user.userId),
                TrendingListCall.generateRegularTrendingCall());
        try {
            return new JSONObject(output);
        } catch (JSONException e) {
            log.error("[User Trending Regular] : Failed to parse output: {}", output, e);
            return null;
        }
    }

    public static ArrayList<Long> getRandomMusicArrayList(){
        ArrayList<Long> musicArrayList = new ArrayList<Long>();
        for(int i=0; i<15; i++){
            musicArrayList.add(Long.valueOf(i));
        }
        Collections.shuffle(musicArrayList);
        return musicArrayList;
    }

    public static ArrayList<Long> getRandomAtmosphereArrayList(){
        ArrayList<Long> musicArrayList = new ArrayList<Long>();
        for(int i=0; i<15; i++){
            musicArrayList.add(Long.valueOf(i));
        }
        Collections.shuffle(musicArrayList);
        return musicArrayList;
    }


    /********************************POST REQUESTS********************************/

    public static boolean addUser() throws IOException, JSONException {
        if(createdUsers.size() >= maxNumberOfUsers)
            return true;

        Map<String, Object> postMap = new HashMap<String, Object>();
        Map<String, Map<String, Object>> msgMap = new HashMap<String, Map<String, Object>>();

        String userName = "";
        for(int i = 0; i< 10; i++){
            userName += alphabet.charAt(rand.nextInt(35));
        }

        postMap.put("userName", userName);
        postMap.put("email", "ignightLoadScript" + rand.nextLong() + rand.nextInt(10000));
        postMap.put("password","password");
        msgMap.put("msg", postMap);
        JSONObject postData = new JSONObject(msgMap);
        String output = HTTPRequestSender.sendPost("/user", postData);

        JSONObject jsonResult = new JSONObject(output);
        if(jsonResult.getBoolean("res")){
            postMap.clear();
            msgMap.clear();

            Long userId = jsonResult.getJSONObject("body").getLong("id");
            Long cityId = UserFunctions.cityId;
            Long genderId = Long.valueOf(rand.nextInt(1));
            Long ageId = Long.valueOf(rand.nextInt(3));

            String url = "/user/" + userId;

            ArrayList<Long> musicArrayList = getRandomMusicArrayList();
            Long[] musicArray = new Long[]{musicArrayList.get(0), musicArrayList.get(1), musicArrayList.get(2)};
            JSONArray music = new JSONArray(musicArray);

            ArrayList<Long> atmosphereArrayList = getRandomAtmosphereArrayList();
            Long[] atmosphereArray = new Long[]{atmosphereArrayList.get(0), atmosphereArrayList.get(1), atmosphereArrayList.get(2)};
            JSONArray atmosphere = new JSONArray(atmosphereArray);

            Long spendingLimit = Long.valueOf(rand.nextInt(3));

            postMap.put("userId", userId.toString());
            postMap.put("cityId", cityId.toString());
            postMap.put("genderId", genderId.toString());
            postMap.put("age", ageId.toString());
            postMap.put("music", music);
            postMap.put("atmosphere", atmosphere);
            postMap.put("spendingLimit", spendingLimit.toString());
            msgMap.put("msg", postMap);

            postData = new JSONObject(msgMap);
            output = HTTPRequestSender.sendPut(url, postData.toString());
            jsonResult = new JSONObject(output);
            if(jsonResult.getBoolean("res"))
                createdUsers.add(userId);
        }
        return jsonResult.getBoolean("res");
    }

    public static boolean updateAge() throws IOException, JSONException{
        Map<String, String> postMap = new HashMap<String, String>();
        Map<String, Map<String, String>> msgMap = new HashMap<String, Map<String, String>>();

        Long userId = createdUsers.get(createdUsers.size() > 1? rand.nextInt(createdUsers.size()-1):0);

        String url = "/user/" + userId;
        Collections.shuffle(createdUsers);

        postMap.put("userId", userId.toString());
        postMap.put("age", Long.valueOf(rand.nextInt(3)).toString());
        msgMap.put("msg", postMap);
        Gson gson = new Gson();
        String postData = gson.toJson(msgMap);
        String output = HTTPRequestSender.sendPost(url, postData);
        JSONObject jsonResult = new JSONObject(output);
        return jsonResult.getBoolean("res");
    }

    public static boolean updateSpendingLimit() throws IOException, JSONException{
        Map<String, String> postMap = new HashMap<String, String>();
        Map<String, Map<String, String>> msgMap = new HashMap<String, Map<String, String>>();

        Collections.shuffle(createdUsers);

        Long userId = createdUsers.get(createdUsers.size() > 1? rand.nextInt(createdUsers.size()-1):0);
        String url = "user/" + userId;

        postMap.put("userId", userId.toString());
        postMap.put("spendingLimit", Long.valueOf(rand.nextInt(3)).toString());
        msgMap.put("msg", postMap);
        Gson gson = new Gson();
        String postData = gson.toJson(msgMap);
        String output = HTTPRequestSender.sendPost(url, postData);
        JSONObject jsonResult = new JSONObject(output);
        return jsonResult.getBoolean("res");
    }

    public static boolean updateMusic() throws IOException, JSONException{
        Map<String, Object> postMap = new HashMap<String, Object>();
        Map<String, Map<String, Object>> msgMap = new HashMap<String, Map<String, Object>>();

        Collections.shuffle(createdUsers);

        Long userId = createdUsers.get(createdUsers.size() > 1? rand.nextInt(createdUsers.size()-1):0);
        String url = "user/" + userId;

        ArrayList<Long> musicArrayList = getRandomMusicArrayList();
        Long[] musicArray = new Long[]{musicArrayList.get(0), musicArrayList.get(1), musicArrayList.get(2)};
        JSONArray music = new JSONArray(musicArray);

        postMap.put("userId", userId.toString());
        postMap.put("music", music);
        msgMap.put("msg", postMap);

        JSONObject postData = new JSONObject(msgMap);
        String output = HTTPRequestSender.sendPost(url, postData);
        JSONObject jsonResult = new JSONObject(output);
        return jsonResult.getBoolean("res");
    }

    public static boolean updateAtmospheres() throws IOException, JSONException{
        Map<String, Object> postMap = new HashMap<String, Object>();
        Map<String, Map<String, Object>> msgMap = new HashMap<String, Map<String, Object>>();

        Collections.shuffle(createdUsers);

        Long userId = createdUsers.get(createdUsers.size() > 1? rand.nextInt(createdUsers.size()-1):0);
        String url = "user/" + userId;

        ArrayList<Long> atmosphereArrayList = getRandomAtmosphereArrayList();
        Long[] atmosphereArray = new Long[]{atmosphereArrayList.get(0), atmosphereArrayList.get(1), atmosphereArrayList.get(2)};
        JSONArray atmosphere = new JSONArray(atmosphereArray);

        postMap.put("userId", userId.toString());
        postMap.put("atmosphere", atmosphere);
        msgMap.put("msg", postMap);

        JSONObject postData = new JSONObject(msgMap);
        String output = HTTPRequestSender.sendPost(url, postData);
        JSONObject jsonResult = new JSONObject(output);
        return jsonResult.getBoolean("res");
    }

    public static boolean updateProfilePicture() throws IOException, JSONException{
        return true;
    }

    /********************************GET REQUESTS********************************/

    public static boolean trendingList() throws IOException, JSONException{

        HashMap<Object, Object> getData = new HashMap<Object, Object>();

        Long userId = createdUsers.get(createdUsers.size() > 1? rand.nextInt(createdUsers.size()-1):0);
        String url = "/trending/" + userId;

        getData.put("userId", userId.toString());
        getData.put("count", "40");

        if(rand.nextBoolean()) {
            Double searchRadius = Double.valueOf(rand.nextDouble() * 20);
            getData.put("search_radius", searchRadius.toString());
            getData.put("latitude", "41.85");
            getData.put("longitude", "-87.65");
        }

        Integer trendingFilter = rand.nextInt(1);
        if(trendingFilter > 0)
            getData.put("upvote_filter", "True");

        if(rand.nextBoolean()){
            Long musicPreference = Long.valueOf(rand.nextInt(14));
            getData.put("music_filter", musicPreference.toString());
        }
        String output = HTTPRequestSender.sendGet(url, getData);
        JSONObject jsonResult = new JSONObject(output);
        return jsonResult.getBoolean("res");
    }

    public static boolean getUserGroups() throws IOException, JSONException{
        HashMap<Object, Object> getData = new HashMap<Object, Object>();

        Long userId = createdUsers.get(createdUsers.size() > 1? rand.nextInt(createdUsers.size()-1):0);
        String url = "/groups/user" + userId;

        String output = HTTPRequestSender.sendGet(url, getData);
        JSONObject jsonResult = new JSONObject(output);
        return jsonResult.getBoolean("res");
    }

    public static boolean searchForUsers() throws IOException, JSONException{
        HashMap<Object, Object> getData = new HashMap<Object, Object>();

        Long userId = createdUsers.get(createdUsers.size() > 1? rand.nextInt(createdUsers.size()-1):0);
        String url = "/groups/users/search/" + userId;

        String search = "";
        Integer searchLength = rand.nextInt(5);
        for(int i = 0; i < searchLength; i++){
            search += alphabet.charAt(rand.nextInt(25));
        }

        getData.put("search", search);

		/*Update to include an actual group*/
        getData.put("groupId", ""+ rand.nextInt(100));
        String output = HTTPRequestSender.sendGet(url, getData);
        JSONObject jsonResult = new JSONObject(output);
        return jsonResult.getBoolean("res");
    }

    @Override
    public void run() {
        int noOfOperations = 100;
        for(int i = 0; i < noOfOperations; i++){
            int randomOperation = rand.nextInt(7);

            try {
                switch (randomOperation){
                    case 1: System.out.println("addUser:" + UserFunctions.addUser());
                        break;

                    case 2: System.out.println("updateAge:" + UserFunctions.updateAge());
                        break;

                    case 3: System.out.println("updateSpendingLimit:" + UserFunctions.updateSpendingLimit());
                        break;

                    case 4: System.out.println("updateMusic:" + UserFunctions.updateMusic());
                        break;

                    case 5: System.out.println("updateAtmospheres:" + UserFunctions.updateAtmospheres());
                        break;

                    case 6: System.out.println("getUserGroups:" + UserFunctions.getUserGroups());
                        break;

                    case 7: System.out.println("searchForUsers:" + UserFunctions.searchForUsers());
                        break;

                    case 0: System.out.println("trendingList:" + UserFunctions.trendingList());
                        break;
                }
            } catch(Exception e) {

            }
        }

    }

}
