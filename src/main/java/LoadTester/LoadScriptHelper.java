package LoadTester;


import java.util.Random;

public class LoadScriptHelper implements Runnable{

    static Random rand = new Random();

    @Override
    public void run() {
        int noOfOperations = 1000;
        int initialNoOfUsers = 50;
        int initialNoOfGroups = 25;

        try{
            for(int k = 0; k < initialNoOfUsers; k++){
                System.out.println("addUser:" + UserFunctions.addUser());
            }

            for(int k = 0; k < initialNoOfGroups; k++){
                System.out.println("addGroup:" + GroupFunctions.addGroup());
            }
        }catch(Exception e){

        }


        for(int i = 0; i < noOfOperations; i++){
            int randomOperation = rand.nextInt(28);

            try{

                switch (randomOperation){

                    case 0: System.out.println("trendingList:" + UserFunctions.trendingList());
                        break;

                    case 1: System.out.println("addUser:" + UserFunctions.addUser());
                        break;

                    case 2: System.out.println("updateAge:" + UserFunctions.updateAge());
                        break;

                    case 3: System.out.println("updateSpendingLimit:" + UserFunctions.updateSpendingLimit());
                        break;

                    case 4: System.out.println("updateMusic:" + UserFunctions.updateMusic());
                        break;

                    case 5: System.out.println("updateAtmospheres:" + UserFunctions.updateAtmospheres());
                        break;

                    case 6: System.out.println("getUserGroups:" + UserFunctions.getUserGroups());
                        break;

                    case 7: System.out.println("searchForUsers:" + UserFunctions.searchForUsers());
                        break;

                    case 8: System.out.println("setUserActivity:" + VenueFunctions.setUserActivity());
                        break;

                    case 9: System.out.println("reportVenueDetailsError:" + VenueFunctions.reportVenueDetailsError());
                        break;

                    case 10: System.out.println("addBuzz:" + VenueFunctions.addBuzz());
                        break;

                    case 11: System.out.println("getVenueDataForUser:" + VenueFunctions.getVenueDataForUser());
                        break;

                    case 12: System.out.println("getLatestBuzz:" + VenueFunctions.getBuzz());
                        break;

                    case 13: System.out.println("getBuzz:" + VenueFunctions.getBuzz());
                        break;

                    case 14: System.out.println("getVenueBuzzImages:" + VenueFunctions.getVenueBuzzImages());
                        break;

                    case 15: System.out.println("requestVenueAddition:" + VenueFunctions.requestVenueAddition());
                        break;

                    case 16: System.out.println("getGroupBuzzImages:" + GroupFunctions.getGroupBuzzImages());
                        break;

                    case 17: System.out.println("addGroup:" + GroupFunctions.addGroup());
                        break;

                    case 18: System.out.println("joinAndLeaveGroup:" + GroupFunctions.joinAndLeaveGroup());
                        break;

                    case 19: System.out.println("inviteUserAndRespondToInvite:" + GroupFunctions.inviteUserAndRespondToInvite());
                        break;

                    case 20: System.out.println("addGroupBuzz:" + GroupFunctions.addGroupBuzz());
                        break;

                    case 21: System.out.println("getListOfUsersInGroup:" + GroupFunctions.getListOfUsersInGroup());
                        break;

                    case 22: System.out.println("searchForGroups:" + GroupFunctions.searchForGroups());
                        break;

                    case 23: System.out.println("popularGroups:" + GroupFunctions.popularGroups());
                        break;

                    case 24: System.out.println("getGroupInfo:" + GroupFunctions.getGroupInfo());
                        break;

                    case 25: System.out.println("trendingList:" + GroupFunctions.trendingList());
                        break;

                    case 26: System.out.println("getIncomingGroupRequests:" + GroupFunctions.getIncomingGroupRequests());
                        break;

                    case 27: System.out.println("getLatestBuzz:" + GroupFunctions.getGroupBuzz());
                        break;

                    case 28: System.out.println("getGroupBuzz:" + GroupFunctions.getGroupBuzz());
                        break;

                }
            }catch(Exception e){

            }
        }

    }

}
