package LoadTester;

import com.google.gson.Gson;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class GroupFunctions implements Runnable{
    public static Long cityId = 0L;
    public static String alphabet = "abcdefghijklmnopqrstuvwxyz";
    public static int maxNumberOfGroups = 500;
    static Random rand = new Random();
    public static ArrayList<Long> createdGroups = new ArrayList<Long>();

    /********************************POST REQUESTS********************************/

    public static boolean addGroup() throws IOException, JSONException{
        if(createdGroups.size() >= maxNumberOfGroups)
            return true;

        Map<String, String> postMap = new HashMap<String, String>();
        Map<String, Map<String, String>> msgMap = new HashMap<String, Map<String, String>>();

        Long userId = UserFunctions.createdUsers.get(UserFunctions.createdUsers.size() > 1? rand.nextInt(UserFunctions.createdUsers.size()-1):0);

        String url = "/group/add/" + userId;

        postMap.put("cityId", cityId.toString());
        postMap.put("groupName", "ignightLoadScriptGroup" + rand.nextLong() + rand.nextInt(10000));
        postMap.put("groupDescription", "Group created by ignight load script. Hope this works.");
        postMap.put("isPrivateGroup", "False");

        msgMap.put("msg", postMap);

        Gson gson = new Gson();
        String postData = gson.toJson(msgMap);
        String output = HTTPRequestSender.sendPost(url, postData);
        JSONObject jsonResult = new JSONObject(output);
        if(jsonResult.getBoolean("res")){
            JSONObject body = jsonResult.getJSONObject("body");
            createdGroups.add(body.getLong("id"));
        }

        return jsonResult.getBoolean("res");
    }

    public static boolean joinAndLeaveGroup() throws IOException, JSONException{
        if(createdGroups.size() <= 0)
            return true;

        Map<String, String> postMap = new HashMap<String, String>();
        Map<String, Map<String, String>> msgMap = new HashMap<String, Map<String, String>>();

        Long userId = UserFunctions.createdUsers.get(UserFunctions.createdUsers.size() > 1? rand.nextInt(UserFunctions.createdUsers.size()-1):0);
        Long groupId = createdGroups.get(createdGroups.size() > 1 ? rand.nextInt(createdGroups.size()-1): 0);

        String url = "/group/join/" + groupId;

        postMap.put("userId", userId.toString());

        msgMap.put("msg", postMap);

        Gson gson = new Gson();
        String postData = gson.toJson(msgMap);
        String output = HTTPRequestSender.sendPost(url, postData);
        JSONObject jsonResult = new JSONObject(output);
        if(jsonResult.getBoolean("res")){
            if(rand.nextBoolean()){
                postMap.clear();
                msgMap.clear();

                url = "/group/join/" + groupId;

                postMap.put("userId", userId.toString());
                msgMap.put("msg", postMap);
                postData = gson.toJson(msgMap);
                output = HTTPRequestSender.sendPost(url, postData);
                jsonResult = new JSONObject(output);
            }
        }
        return jsonResult.getBoolean("res");
    }

    public static boolean inviteUserAndRespondToInvite() throws IOException, JSONException{
        if(createdGroups.size() <= 0)
            return true;

        if(UserFunctions.createdUsers.size() <= 1)
            return true;

        Map<String, String> postMap = new HashMap<String, String>();
        Map<String, Map<String, String>> msgMap = new HashMap<String, Map<String, String>>();

        Long userId = UserFunctions.createdUsers.get(UserFunctions.createdUsers.size() > 1? rand.nextInt(UserFunctions.createdUsers.size()-1):0);
        Long groupId = createdGroups.get(createdGroups.size() > 1 ? rand.nextInt(createdGroups.size()-1): 0);

        String url = "/group/invite/" + groupId;

        Long targetUserId = -1L;

        for(int i = 0; i < UserFunctions.createdUsers.size(); i++){
            if(UserFunctions.createdUsers.get(i) != userId){
                targetUserId = UserFunctions.createdUsers.get(i);
                break;
            }
        }

        postMap.put("userId", userId.toString());
        postMap.put("targetUserId", targetUserId.toString());
        msgMap.put("msg", postMap);

        Gson gson = new Gson();
        String postData = gson.toJson(msgMap);
        String output = HTTPRequestSender.sendPost(url, postData);
        JSONObject jsonResult = new JSONObject(output);
        if(jsonResult.getBoolean("res")){
            postMap.clear();
            msgMap.clear();

            url = "/invite/respond/" + groupId;

            postMap.put("userId", targetUserId.toString());
            postMap.put("accept", "" + rand.nextBoolean());
            msgMap.put("msg", postMap);

            postData = gson.toJson(msgMap);
            output = HTTPRequestSender.sendPost(url, postData);
            jsonResult = new JSONObject(output);
        }
        return jsonResult.getBoolean("res");
    }

    /*Add image upload later*/
    public static boolean addGroupBuzz() throws IOException, JSONException{
        if(createdGroups.size() <= 0)
            return true;

        Map<String, String> postMap = new HashMap<String, String>();
        Map<String, Map<String, String>> msgMap = new HashMap<String, Map<String, String>>();

        Long userId = UserFunctions.createdUsers.get(UserFunctions.createdUsers.size() > 1? rand.nextInt(UserFunctions.createdUsers.size()-1):0);
        Long groupId = createdGroups.get(createdGroups.size() > 1 ? rand.nextInt(createdGroups.size()-1): 0);

        String url = "/buzz/add/group/text/" + groupId;

        postMap.put("userId", userId.toString());
        postMap.put("buzzText",  "random buzz text");

        msgMap.put("msg", postMap);
        Gson gson = new Gson();
        String postData = gson.toJson(msgMap);
        String output = HTTPRequestSender.sendPost(url, postData);
        JSONObject jsonResult = new JSONObject(output);
        return jsonResult.getBoolean("res");
    }

    /********************************GET REQUESTS********************************/

    public static boolean getListOfUsersInGroup() throws IOException, JSONException{
        if(createdGroups.size() <= 0)
            return true;

        Map<Object, Object> getData = new HashMap<Object, Object>();

        Long userId = UserFunctions.createdUsers.get(UserFunctions.createdUsers.size() > 1? rand.nextInt(UserFunctions.createdUsers.size()-1):0);
        Long groupId = createdGroups.get(createdGroups.size() > 1 ? rand.nextInt(createdGroups.size()-1): 0);

        String url = "/group/members/" + groupId;

        getData.put("userId", userId.toString());

        String output = HTTPRequestSender.sendGet(url, getData);
        JSONObject jsonResult = new JSONObject(output);
        return jsonResult.getBoolean("res");
    }

    public static boolean searchForGroups() throws IOException, JSONException{
        if(createdGroups.size() <= 0)
            return true;

        Map<Object, Object> getData = new HashMap<Object, Object>();

        Long userId = UserFunctions.createdUsers.get(UserFunctions.createdUsers.size() > 1? rand.nextInt(UserFunctions.createdUsers.size()-1):0);

        String url = "/group/search/" + userId;

        String search = "";
        Integer searchLength = rand.nextInt(5);
        for(int i = 0; i < searchLength; i++){
            search += alphabet.charAt(rand.nextInt(25));
        }

        getData.put("search", search);

        String output = HTTPRequestSender.sendGet(url, getData);
        JSONObject jsonResult = new JSONObject(output);
        return jsonResult.getBoolean("res");
    }

    public static boolean popularGroups() throws IOException, JSONException{
        Map<Object, Object> getData = new HashMap<Object, Object>();

        Long userId = UserFunctions.createdUsers.get(UserFunctions.createdUsers.size() > 1? rand.nextInt(UserFunctions.createdUsers.size()-1):0);

        String url = "/group/popular/" + userId;

        getData.put("count", "" + rand.nextInt(25));

        String output = HTTPRequestSender.sendGet(url, getData);
        JSONObject jsonResult = new JSONObject(output);
        return jsonResult.getBoolean("res");
    }

    public static boolean getGroupInfo() throws IOException, JSONException{
        if(createdGroups.size() <= 0)
            return true;

        Map<Object, Object> getData = new HashMap<Object, Object>();

        Long userId = UserFunctions.createdUsers.get(UserFunctions.createdUsers.size() > 1? rand.nextInt(UserFunctions.createdUsers.size()-1):0);
        Long groupId = createdGroups.get(createdGroups.size() > 1 ? rand.nextInt(createdGroups.size()-1): 0);

        String url = "/group/info/" + groupId;

        getData.put("userId", userId.toString());

        String output = HTTPRequestSender.sendGet(url, getData);
        JSONObject jsonResult = new JSONObject(output);
        return jsonResult.getBoolean("res");
    }

    public static boolean trendingList() throws IOException, JSONException{
        if(createdGroups.size() <= 0)
            return true;

        Map<Object, Object> getData = new HashMap<Object, Object>();

        Long userId = UserFunctions.createdUsers.get(UserFunctions.createdUsers.size() > 1? rand.nextInt(UserFunctions.createdUsers.size()-1):0);
        Long groupId = createdGroups.get(createdGroups.size() > 1 ? rand.nextInt(createdGroups.size()-1): 0);

        String url = "/group/trending/" + groupId;

        getData.put("userId", userId.toString());

        String output = HTTPRequestSender.sendGet(url, getData);
        JSONObject jsonResult = new JSONObject(output);
        return jsonResult.getBoolean("res");
    }

    public static boolean getIncomingGroupRequests() throws IOException, JSONException{

        Map<Object, Object> getData = new HashMap<Object, Object>();

        Long userId = UserFunctions.createdUsers.get(UserFunctions.createdUsers.size() > 1? rand.nextInt(UserFunctions.createdUsers.size()-1):0);

        String url = "/inbox/" + userId;

        String output = HTTPRequestSender.sendGet(url, getData);
        JSONObject jsonResult = new JSONObject(output);
        return jsonResult.getBoolean("res");
    }

	/*
	public static boolean getLatestBuzz() throws IOException, JSONException{
		HashMap<String, String> getData = new HashMap<String, String>();

		getData.put("type", "getLatestBuzz");
		getData.put("userId", UserFunctions.createdUsers.get(UserFunctions.createdUsers.size() > 1? rand.nextInt(UserFunctions.createdUsers.size()-1):0).toString());
		getData.put("groupId", "" + createdGroups.get(createdGroups.size() > 1 ? rand.nextInt(createdGroups.size()-1): 0));
		getData.put("cityId", cityId.toString());
		getData.put("lastBuzzId", "-1");
		getData.put("count", "" + (rand.nextInt(25) + 1));

		String output = HTTPRequestSender.sendGet(urlString, getData);
		JSONObject jsonResult = new JSONObject(output);
		return jsonResult.getBoolean("res");
	}
	*/

    public static boolean getGroupBuzz() throws IOException, JSONException {
        Map<Object, Object> getData = new HashMap<Object, Object>();

        Long userId = UserFunctions.createdUsers.get(UserFunctions.createdUsers.size() > 1? rand.nextInt(UserFunctions.createdUsers.size()-1):0);
        Long groupId = createdGroups.get(createdGroups.size() > 1 ? rand.nextInt(createdGroups.size()-1): 0);

        String url = "/buzz/group/" + groupId;
        getData.put("userId", userId.toString());
        getData.put("lastBuzzId", "-1");
        getData.put("count", "" + (rand.nextInt(25) + 1));
        getData.put("latest", rand.nextBoolean() ? "1" : "0");

        String output = HTTPRequestSender.sendGet(url, getData);
        JSONObject jsonResult = new JSONObject(output);
        return jsonResult.getBoolean("res");
    }

    public static boolean getGroupBuzzImages() throws IOException, JSONException{
        Map<Object, Object> getData = new HashMap<Object, Object>();

        Long groupId = createdGroups.get(createdGroups.size() > 1 ? rand.nextInt(createdGroups.size()-1): 0);

        String url = "/images/group/" + groupId;

        getData.put("buzzImageStartId", "0");
        getData.put("noOfBuzzImages", "" + rand.nextInt(50));

        String output = HTTPRequestSender.sendGet(url, getData);
        JSONObject jsonResult = new JSONObject(output);
        return jsonResult.getBoolean("res");
    }

    @Override
    public void run() {
        int noOfOperations = 100;
        for(int i = 0; i < noOfOperations; i++){
            int randomOperation = rand.nextInt(12);
            try{
                switch (randomOperation){
                    case 0: System.out.println("getGroupBuzzImages:" + GroupFunctions.getGroupBuzzImages());
                        break;

                    case 1: System.out.println("addGroup:" + GroupFunctions.addGroup());
                        break;

                    case 2: System.out.println("joinAndLeaveGroup:" + GroupFunctions.joinAndLeaveGroup());
                        break;

                    case 3: System.out.println("inviteUserAndRespondToInvite:" + GroupFunctions.inviteUserAndRespondToInvite());
                        break;

                    case 4: System.out.println("addGroupBuzz:" + GroupFunctions.addGroupBuzz());
                        break;

                    case 5: System.out.println("getListOfUsersInGroup:" + GroupFunctions.getListOfUsersInGroup());
                        break;

                    case 6: System.out.println("searchForGroups:" + GroupFunctions.searchForGroups());
                        break;

                    case 7: System.out.println("popularGroups:" + GroupFunctions.popularGroups());
                        break;

                    case 8: System.out.println("getGroupInfo:" + GroupFunctions.getGroupInfo());
                        break;

                    case 9: System.out.println("trendingList:" + GroupFunctions.trendingList());
                        break;

                    case 10: System.out.println("getIncomingGroupRequests:" + GroupFunctions.getIncomingGroupRequests());
                        break;

                    case 11: System.out.println("getLatestBuzz:" + GroupFunctions.getGroupBuzz());
                        break;

                    case 12: System.out.println("getGroupBuzz:" + GroupFunctions.getGroupBuzz());
                        break;

                }
            }catch(Exception e){

            }
        }

    }

}